# Weekly Rhythm

Agaric's weekly communication is structured like this:

 * [Monday check-in](monday-checkin)
 * Tuesday worker-owner meeting
 * [Wednesday check-in](wednesday-checkin)
 * [Friday review & planning](friday-review-and-planning)

One hour is the maximum time for any meeting.

No (internal co-op-wide) meetings on Thursdays.

Check-in meetings are at 11:30am.

Tuesday worker-owner meetings are *as needed* at 1pm.  The worker-owner meeting happens weekly with an agenda in advance. If there are no topics put forward to discuss by the day before it is skipped.  There is no check-in (stand up) meeting on Tuesday.

```{toctree}
---
caption: Weekly communication templates
maxdepth: 2
---

monday-checkin
wednesday-checkin
friday-review-and-planning
```
