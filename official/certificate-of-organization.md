# Certificate of Organization

Agaric Technology Collective's Certificate of Organization filed with the Commonwealth of Massachusetts (most states call the equivalent document "Articles of Organization") reads:

> 1. The exact name of the limited liability company is: **AGARIC, LLC**

> 3. The general character of business, and if the limited liability company is organized to render professional service, the service to be rendered:
>
> **AGARIC HELPS PEOPLE CREATE AND USE ONLINE TOOLS AND PLATFORMS THAT MEET
THEIR AND THEIR COMMUNITIES' NEEDS. THE COLLECTIVE GOAL IS THE MOST POWER
POSSIBLE FOR ALL PEOPLE OVER THEIR OWN LIVES, AND TOWARD THAT GOAL THE
COLLECTIVE TAKES ON WEB DEVELOPMENT PROJECTS THAT CONNECT IDEAS,
RESOURCES, AND PEOPLE. WE USE AND CONTRIBUTE TO OPEN SOURCE FREE SOFTWARE
AND ENSURE EVERYTHING WE BUILD IS FREE, AND THE WAY WE BUILD IT OPEN, FOR OUR
CLIENTS TO TRULY OWN.**

In Massachusetts, member-management is the default structure for an LLC.  (Other state forms would have required choosing between member-managed and manager-managed.)
