## Operating agreement for Agaric, LLC

[Note: For a corporation, these would be called bylaws.]

## ARTICLE I: Company Formation

### 1.1 FORMATION.

The Members hereby form a worker owned and run Limited Liability Company ("Collective") subject to the provisions of the Limited Liability Company Act as currently in effect as of this date, 2011 May 18. Articles of Organization shall be filed with the Secretary of the Commonwealth.

### 1.2 NAME.

The name of the Collective shall be Agaric.

### 1.3 REGISTERED AGENT.

Up-to-date information can be found at:

https://corp.sec.state.ma.us/CorpWeb/CorpSearch/CorpSummary.aspx?sysvalue=HjjIHbOP890XgjC6fWoReGpnXzMPqiTgvN_a5t8CxoA-

### 1.4 TERM.

The Collective shall continue for a perpetual period.

(a) Members whose capital interest as defined in Article 2.2 exceeds 50 percent vote for
dissolution; or
(b) Any event which makes it unlawful for the business of the Collective to be carried on
by the Members; or
(c) The death, resignation, expulsion, bankruptcy, retirement of a Member or the
occurrence of any other event that terminates the continued membership of a Member
of the Collective; or
(d) Any other event causing dissolution of this Limited Liability Company under the laws
of the Commonwealth of Massachusetts.

### 1.5 CONTINUANCE OF COMPANY.

Notwithstanding the provisions of ARTICLE 1.4, in the event of an occurrence described in ARTICLE 1.4(c), if there are at least two remaining Members, said remaining Members shall have the right to continue the business of the Collective. Such right can be exercised only by the unanimous vote of the remaining Members within ninety (90) days after the occurrence of an event described in ARTICLE 1.4(c). If not so exercised, the right of the Members to continue the business of the Collective shall expire.

### 1.6 BUSINESS PURPOSE.

The purpose of the Collective is to help people create and use tools and platforms that meet their needs.  The Collective goal is the most power possible for all people over their own lives, and toward that goal the Collective takes on projects that connect ideas, resources, and people.  The Collective uses and contributes to open source free software and ensure everything built free from restrictions, and the way it is built open, for clients to truly own.

### 1.7 PRINCIPAL PLACE OF BUSINESS.

The location of the principal place of business of the Collective shall be:

Boston, MA 02134

Principal place of business may be changed from time to time to a location the Managers select. Members may also use home-offices or any other workplace of their choice.

### 1.8 THE MEMBERS.

The name and place of residence of each Member is located at https://corp.sec.state.ma.us/CorpWeb/CorpSearch/CorpSummary.aspx?sysvalue=HjjIHbOP890XgjC6fWoReGpnXzMPqiTgvN_a5t8CxoA-

### 1.9 ADMISSION OF ADDITIONAL MEMBERS.

Except as otherwise expressly provided in the Agreement, no additional members may be admitted to the Collective through issuance by the Collective of a new interest in the Collective, without the prior unanimous written consent of the Members.


## ARTICLE II Capital Contributions

### 2.1 INITIAL CONTRIBUTIONS.

The Members initially shall contribute to the Company capital as described in Exhibit 3 attached to this Agreement. The agreed total value of such property and cash is $18,000.

### 2.2 ADDITIONAL CONTRIBUTIONS.

Except as provided in ARTICLE 6.2, no Member shall be obligated to make any additional contribution to the Company's capital.

## ARTICLE III Profits, Losses and Distributions

### 3.1 PROFITS/LOSSES.

For financial accounting and tax purposes the Company's net profits or net losses shall be determined on an annual basis and shall be allocated to the Members in proportion to each Member's relative capital interest in the Company as set forth in Exhibit 2 as amended from time to time in accordance with Treasury Regulation 1.704-1.

### 3.2 DISTRIBUTIONS.

The Members shall determine and distribute available funds annually or at more frequent intervals as they see fit. Available funds, as referred to herein, shall mean the net cash of the Company available after appropriate provision for expenses and liabilities, as determined by the Managers. Distributions in liquidation of the Company or in liquidation of a Member's interest shall be made in accordance with the positive capital account balances pursuant to Treasury Regulation 1.704-l(b)(2)(ii)(b)(2). To the extent a Member shall have a negative capital account balance, there shall be a qualified income offset, as set forth in Treasury Regulation 1.704-l(b)(2)(ii)(d).


### ARTICLE IV: Management

### 4.1 MANAGEMENT OF THE BUSINESS.

The name and place of residence of each Manager is located at https://corp.sec.state.ma.us/CorpWeb/CorpSearch/CorpSummary.aspx?sysvalue=HjjIHbOP890XgjC6fWoReGpnXzMPqiTgvN_a5t8CxoA-

Each Member is automatically a Manager.  By a vote of the Members holding a majority of the capital interests in the Collective, as set forth in Schedule 2 as amended from time to time, shall elect so many additional non-Member Managers as the Members determine.

### 4.2 MEMBERS.

The liability of the Members shall be limited as provided pursuant to applicable law.  No Member shall be an agent of any other Member of the Collective solely by reason of being a Member.

### 4.3 POWERS OF MANAGERS.

The Managers are authorized on the Collective's behalf to make all decisions as to
(a) the sale, development lease or other disposition of the Collective's assets;
(b) the purchase or other acquisition of other assets of all kinds;
(c) the management of all or any part of the Collective's assets;
(d) the borrowing of money and the granting of security interests in the Collective's assets;
(e) the pre-payment, refinancing or extension of any loan affecting the Company's assets;
(f) the compromise or release of any of the Collective's claims or debts; and,
(g) the employment of persons, firms or corporations for the operation and management of the company's business.

In the exercise of their management powers, the Collective are authorized to execute and deliver
(a) all contracts, conveyances, assignments leases, sub-leases, franchise agreements, licensing agreements, management contracts and maintenance contracts covering or affecting the Collective's assets;
(b) all checks, drafts and other orders for the payment of the Collective's funds;
(c) all promissory notes, loans, security agreements and other similar documents; and,
(d) all other instruments of any other kind relating to the Collective's affairs, whether like or unlike the foregoing.

### 4.4 NOMINEE.

Title to the Collective's assets shall be held in the Collective's name or in the name of any nominee that the Managers may designate. The Managers shall have power to enter into a nominee agreement with any such person, and such agreement may contain provisions indemnifying the nominee, except for his willful misconduct.

### 4.6 COMPANY INFORMATION.

Upon request, the Managers shall supply to any member information regarding the Collective or its activities. Each Member or his authorized representative shall have access to and may inspect and copy all books, records and materials in the Manager's possession regarding the Company or its activities. The exercise of the rights contained in this ARTICLE 4.6 shall be at the requesting Member's expense.

### 4.7 EXCULPATION.

Any act or omission of the Managers, the effect of which may cause or result in loss or damage to the Company or the Members if done in good faith to promote the best interests of the Company, shall not subject the Managers to any liability to the Members.

### 4.8 INDEMNIFICATION.

The Company shall indemnify any person who was or is a party defendant or is threatened to be made a party defendant, pending or completed action, suit or proceeding, whether civil, criminal, administrative, or investigative (other than an action by or in the right of the Company) by reason of the fact that he is or was a Member of the Company, Manager, employee or agent of the Company, or is or was serving at the request of the Company, for instant expenses (including attorney's fees), judgments, fines, and amounts paid in settlement actually and reasonably incurred in connection with such action, suit or proceeding if the Members determine that he acted in good faith and in a manner he reasonably believed to be in or not opposed to the best interest of the Company, and with respect to any criminal action proceeding, has no reasonable cause to believe his/her conduct was unlawful. The termination of any action, suit, or proceeding by judgment, order, settlement, conviction, or upon a plea of "no lo Contendere" or its equivalent, shall not in itself create a presumption that the person did or did not act in good faith and in a manner which he reasonably believed to be in the best interest of the Company, and, with respect to any criminal action or proceeding, had reasonable cause to believe that his/her conduct was lawful.

### 4.9 RECORDS.

The Managers shall cause the Company to keep at its principal place of business the following:
(a) a current list in alphabetical order of the full name and the last known street address
of each Member;
(b) a copy of the Certificate of Formation and the Company Operating Agreement and all
amendments;
(c) copies of the Company's federal, state and local income tax returns and reports, if
any, for the three most recent years;
(d) copies of any financial statements of the limited liability company for the three most
recent years.

## ARTICLE V: Compensation

### 5.1 MANAGEMENT FEE.

Any Manager rendering services to the Collective shall be entitled to compensation commensurate with the value of such services.

### 5.2 REIMBURSEMENT.

The Collective shall be able to establish policies for full or partial reimbursement to the Managers or Members for direct out-of-pocket expenses incurred by them in managing the Company.

## ARTICLE VI: Bookkeeping

### 6.1 BOOKS.

The Managers shall maintain complete and accurate books of account of the Collective's affairs at the Collective's principal place of business. Such books shall be kept on such method of accounting as the Managers shall select. The company's accounting period shall be the calendar year.

### 6.2 MEMBER'S ACCOUNTS.

The Managers shall maintain separate capital and distribution accounts for each member. Each member's capital account shall be determined and maintained in the manner set forth in Treasury Regulation 1.704-l(b)(2)(iv) and shall consist of his initial capital contribution increased by:
(a) any additional capital contribution made by him/her;
(b) credit balances transferred from her distribution account to her capital account;
and decreased by:
(c) distributions to him/her in reduction of Collective capital;
(d) the Member's share of Collective losses if charged to his/her capital account.

### 6.3 REPORTS.

The Managers shall close the books of account after the close of each calendar year, and shall prepare and send to each member a statement of such Member's distributive share of income and expense for income tax reporting purposes.

## ARTICLE VII: Transfers

### 7.1 ASSIGNMENT.

If at any time a Member proposes to sell, assign or otherwise dispose of all or any part of her interest in the Collective, such Member shall first make a written offer to sell such interest to the other Members at a price determined by mutual agreement. If such other Members decline or fail to elect such interest within thirty (30) days, and if the sale or assignment is made and the Members fail to approve this sale or assignment unanimously then, pursuant to the applicable law, the purchaser or assignee shall have no right to participate in the management of the business and affairs of the Collective. The purchaser or assignee shall only be entitled to receive the share of the profits or other compensation by way of income and the return of contributions to which that Member would otherwise be entitled.


## LISTING OF MEMBERS

An up-to-date list of Members of the Collective is located at:

https://corp.sec.state.ma.us/CorpWeb/CorpSearch/CorpSummary.aspx?sysvalue=HjjIHbOP890XgjC6fWoReGpnXzMPqiTgvN_a5t8CxoA-
