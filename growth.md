# Growing Agaric

As a mushroom, Agaric has several options for growth.  We don't want to become [the largest organism on the planet](https://www.scientificamerican.com/article/strange-but-true-largest-organism-is-fungus/) so our approach is [mitosis](https://www.sciencedirect.com/science/article/abs/pii/S0074769608600790)— dividing.

If or when Agaric grows to more than about a dozen worker-owners, we plan to split into two equal groups, opearting under the banner of Agaric and sharing resources, but federating as small worker cooperatives rather than changing our basic internal structure to scale.
