# Content entry on Agaric's own sites

*Or, a guide to Agaric's experimental branch of Drutopia.*

First, familiarize yourself with Agaric's tone and voice as described in [Agaric's content style guide](/content-style-guide.md).

Second, add content!  Most commonly, this will be blog posts.

## Writing Blog Posts

* Use title case
* Keep summary text to one sentence when possible.
* Write a summary that gives a glimpse into the content, piquing the reader's interest
* Use headings and lists to break up content
* Include pictures and screenshots when possible
* Use captions to highlight key points of the post

## Use Title paragraphs on Case Studies and certain pages

To give control over the header layout to content editors, we've created a paragraph type called 'Title'.  If it is present on content being shown as a full page, it uses its image, title, and subtitle to replace the usual header and page title section.

