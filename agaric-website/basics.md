# Agaric Website Basics

Agaric's main web site is built on [Drutopia](https://drutopia.org/) and most of the [Drutopia End User Documentation](http://docs.drutopia.org/en/latest/end-user-documentation.html) will apply.

## Quirks to watch out for

* Use `alt + shift + g` keyboard shortcut to search through administrative options.  There's no menu for editors (it conflicts with the theme right now), sorry!
* All case studies should use a title paragraph, and you must select a color for it.
* For now, don't select any other 'styles' on any paragraph type unless you know what you're doing (documentation to come).

## Posting blogs

* The main image, like the summary, is used only for teasers (blog listings, search results).  It is therefore requested that you incorporate this image into the early content of your 
* Do *not* use a title paragraph for blogs, in general, as we don't currently have a way of showing the author and date posted information.

```{seealso}
[Agaric's Content Style Guide](/content-style-guide.md)
```
