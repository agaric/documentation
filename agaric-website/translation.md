# Translating Agaric.coop

## Doing translation

Agaric.coop is built on Drutopia, and continues Drutopia's heavy use of Paragraphs module.

[Drupal Paragraphs pose special challanges to content translation](https://www.drupal.org/docs/8/modules/paragraphs/multilingual-paragraphs-configuration ), but most of that is in the configuration, and once its set up everything is good.

Even though you *can* select a different Webform from the referencing paragraph for each language, you shouldn't.

> there was some limitation with webform and translations...I think the confirmation message is not translatable.

If that is the case, it doesn't matter because it's better not to use confirmation messages at all.  Instead, for caching and for analytics (tracking a submitted webform) purposes, it's better to have webforms redirect to a confirmation node on submit, and these can be translated as usual.
