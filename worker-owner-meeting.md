# Worker-Owner Meeting

Copy-pastable template:

The worker-owner meeting should mostly be populated with agenda items.

It replaces the daily standup if it falls on the same day as one, but ideally we would separate daily items to the daily/weekly rythm pad and leave the worker-owner notes with only the higher-level subjects.  Switching pads during the meeting is too disruptive but maybe moving check-ins and work updates to the daily pad after the meeting would work.

```
# 2024 November 24

## Check-ins (i.e. how are you, generally?)

* Ben 
* Chris
* Sanjay
* Mauricio 
* Micky 
* Keegan
* Louis

## Financial

Checking: 
Savings: 
Credit: 

### Payable
### Receivable
### Transactions 


## Marketing

### Blog Posts

## Training

## Decisions to make

## Good News
* Ben
* Chris
* Sanjay
* Mauricio 
* Micky
* Keegan
* Louis
```
