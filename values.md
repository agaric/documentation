# Values

Combined with [our purpose](purpose) and [the cooperative principles](cooperative-principles) these values guide our daily work and long-term strategy.

* We prioritize health and family over work.
* We work for the benefit of the group.
* We encourage continuous learning.
* We appreciate and welcome new ideas.
* We give back to the communities we are part of.
* We value long term relationships.

```{note}
Proposed by [Mauricio Dinarte](http://agaric.com/people/mauricio-dinarte).
```

* We support libre software as one pillar of human freedom and progress that is also core to our work.
* We lend our talents to movements for liberation

```{note}
Proposed by [Benjamin Melançon](http://agaric.coop/mlncn).
```
