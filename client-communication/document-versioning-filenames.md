# Document versioning and filenames

Please use the following file naming convention for all client-facing filenames:

```
agaric-[client-name]-[purpose]-[yyyy-mm-dd].ext
```

For example:

```
agaric-acme-jetpacks-proposal-2024-05-23.pdf
```

Always use dates, do not use revision numbers, and never use 'final' in a file name.  A final-final-FINAL doc is only funny a couple times.

This applies to the final sent versions of files, typically PDFs.  The word processing document (.odt, .doc) or Markdown (.md) working versions should follow the same format for their file names, for convenience, but leave off the date.

For more on document naming reasoning see [Orr Shtuhl in *A List Apart* on avoiding trouble by making deliverables easy to find](https://alistapart.com/article/looking-for-trouble/#section2).
