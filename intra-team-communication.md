# In-team communication

Tasks people need to do go in GitLab, but that is *not* how tasks should be communicated.  Whenever possible, anything important enough to be assigned is worth talking about in 'real-time' first, or concomitantly.  Everything needed to understand an issue should be captured in GitLab, but we want to avoid back-and-forth 'huh what?' and 'nuh-uh not me' (un-assign)

Discussion that takes place outside of GitLab needs to be summarized (or copy-pasted) into the relevant issue in the GitLab project.

Try to get important meetings, internal as well as external, on your [Agaric calendar](calendars).

## Zulip

Our primary tool for internal communication is now [Zulip](https://zulipchat.com/), free software group chat software available as [LibreSaaS](https://libresaas.org/).

[agaric.zulipchat.com](https://agaric.zulipchat.com) is our web interface.

[Downloading an application](https://zulipchat.com/apps/) is recommended.


### E-mailing messages to Zulip

When BCCing a Zulip stream on a message to someone, Use the default that can be found under stream settings. First press 'Channel settings' menu item in 'Main menu' (gear icon in top right), then filter for your channel and finally press the 'Generate email address' to reveal the address. Although the button says 'Generate email address' the same addresses are used so it is more a revelation than a generation:

```
project.785f16fc671a5d8c0f2d4fbb161f16b3.show-sender@streams.zulipchat.com
```

When forwarding an e-mail to a Zulip stream, swap out `.show-sender` for `.include-footer.include-quotes.prefer-html`

```
project.785f16fc671a5d8c0f2d4fbb161f16b3.include-footer.include-quotes.prefer-html@streams.zulipchat.com
```

The subject of the email message will be used as the topic name in the stream.

```{info}
More about [sending a message to a Zulip stream by e-mail](https://zulipchat.com/help/message-a-stream-by-email)
```

Hover over a stream in the lefthand column to get to stream settings; there's also a link to a list of all streams that is easier: https://agaric.zulipchat.com/#streams/all

Stream settings pages are also key for getting the e-mail address for sending e-mail to streams, https://agaric.zulipchat.com/help/message-a-stream-by-email (their help is lightly customized to organization, so that "Your streams" link will work)


## Real-time meetings with BigBlueButton

Agaric uses BigBlueButton (BBB) video chat for our daily Stand Up meetings and for client meetings. We have an account with [meet.coop](https://meet.coop) but currently primarily use our own instance, [meet.agaric.coop](https://meet.agaric.coop).

Regular meetings include [Monday](monday-checkin.md), [Wednesday](wednesday-checkin.md), and [Friday](friday-review-and-planning.md) [weekly rhythm meetings](weekly-rhythm.md).  More irregular meetings include as-needed [worker-owner meetings](worker-owner-meeting.md).

## IRC

\#agaric

Agaric also maintains a channel for worker-owners only. NOTE: We have not been using IRC sine Zulip arrived, but some of us still hang out there and use it to connect with other developers that are not in Zulip.

Freenode has given us operator privileges for this channel.  To use it, we need to register:

```
/msg nickserv identify mlncn pa55w0rd
/chanserv op #agaric mlncn
```

Some of our best clients are also on IRC, as are our partners at [May First Movement Technology (#mayfirst on irc.indymedia.org)](https://support.mayfirst.org/wiki/faq/chat).

## Internal notes

Agarics can get more detail [on communication channels in the wiki](https://gitlab.com/agaric/internal/wikis/Communication-Channels).
