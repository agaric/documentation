# Days off

Collective-wide holidays.

Federal holidays:

 * New Year's Day
 * Martin Luther King Jr. Day
 * Presidents' Day
 * Memorial Day
 * Juneteenth
 * Independence Day
 * Labor Day
 * Columbus Day
 * Armistice Day (kids these days call it Veterans Day)
 * Thanksgiving Day
 * Christmas Day

 And the real labor day:
 * May First

Source: [opm.gov/policy-data-oversight/pay-leave/federal-holidays](https://www.opm.gov/policy-data-oversight/pay-leave/federal-holidays/)

Given that most of these move around year to year to be the closest Monday, we have added the .ics file OPM provided (goes through 2030) to the [Agaric shared calendar](calendars) via Thunderbird's "Events & Tasks » Import".
