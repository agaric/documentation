# Drupal Module Project Page template

*This is what Drupal provides as a prompt for the description of a new module:*

```html
Here, write an introduction that summarizes the purpose and function of this project with a focus on users brand new to Drupal. Answer the question: What solution does this module provide? The first 200 characters of this will be shown when browsing projects. Alternatively, you can click “Edit summary” above and enter the exact summary you want (it should be 200 characters or less).

<h3 id="module-project--features">Features</h3>
Here, answer the following questions: What is the basic functionality? What unique features does enabling this project add? When and why would someone use this module? What use cases are there?

<h3 id="module-project--post-installation">Post-Installation</h3>
How does this module actually work once I install it? Should I go to a config page? Should I look for a new content type? Should I go and manage my text formats? Provide an overview of the configuration process and any other special considerations for the module.

<h3 id="module-project--additional-requirements">Additional Requirements</h3>
Does this project need anything beyond Drupal core? Include any dependent modules, libraries, APIs, etc., that are required for this project to work.

<h3 id="module-project--recommended-libraries">Recommended modules/libraries</h3>
Are there any projects that enhance or improve the functionality of this project?

<h3 id="module-project--similar-projects">Similar projects</h3>
If there are modules providing similar functionality, please describe what differentiates them.

<h3 id="module-project--support">Supporting this Module</h3>
If you have a Patreon, OpenCollective, etc. you can put links here to describe how people can support development. 

<h3 id="module-project--community-documentation">Community Documentation</h3>
A great place to add links to YouTube walkthroughs, external documentation, or a demo site (use DrupalPod!).

You may continue to put additional information below here, if there are other things you think people need to know about your module!
```

*For financial support, a good default for our modules is:*

```html
You can support <a href="https://agaric.coop/">Agaric's</a> overall contributions to Drupal and a bit beyond <a href="https://opencollective.com/drutopia">by supporting Drutopia at opencollective.com/drutopia</a>.  Thanks!!
```
