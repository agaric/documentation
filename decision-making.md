# Decision-making

Meetings should be short and full of as much human connection as possible.

Moving decisions to asynchronous processes and sub-groups helps with that.

(This is still more aspiration than reality for us, and we've tried [Loomio](https://loomio.org) a few times!)

  * Leads shall be responded to promptly without gathering the full team's input.  Ask for the input of one or two people if necessary for their knowledge or perspective.
  * Even accepting a project only requires the explicit assent of the people expected to work on it.
  * Our goal is always to act with *transparency* and *revisability*— anyone may make any decision that is not irrevocable.

For now, 


## Monthly worker-owner meetings

Set agenda ahead of time

Choose facilitator at start of meeting, or by predetermined system

Facilitator's job is to keep things moving.

Someone other than the facilitator should be designated for taking notes, but everyone can help (we will always do meeting notes on a text pad allowing real-time collaborative note-taking, such as [Etherpad](https://etherpad.org/), the shared notes built into [Big Blue Button](https://bigbluebutton.org/), or [HedgeDoc](https://hedgedoc.org/) (this is our current main tool for notes, which we self-host).

See template at [worker-owner meeting](worker-owner-meeting).
