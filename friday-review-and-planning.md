# Friday Shipping Meeting

```md
# 2023 {Date}, Friday shipping

## Major Accomplishments/Hurdles Cleared?

## Updates

### Ben
* 

### Chris
* 

### Keegan
* 

### Mauricio
* 

### Micky
* 

### Louis
*


## Availability

*Please note just any business hours or other necessities you may not be able to be present for next week.*

## Project updates

*What projects need our attention in the coming week?*

## Learning Round

* Neat tips & tricks, or nasty bugs squashed

## Thanks

* Ben - 
* Chris - 
* Keegan - 
* Mauricio - 
* Micky - 
* Louis
```
