# Agaric's Content Style Guide

Please reference this styleguide when writing for Agaric to keep our communications clear and consistent.

## Summary

Good content is authentic, useful, and appropriate to its context.

Agaric's voice is straightforward, bold, and irreverant.

[Write for translation](#writing-for-translation) by writing clear, straightforward English.  [Write for accessibility](#writing-for-accessibility) by doing the same and by organizing information, steering clear of distractions, and being mindful of alternative ways of processing information (including non-visual).

  * Group related ideas together with descriptive headers and subheaders.
  * Lead with the main point.
  * Use active voice and positive language.
  * Use short words and sentences.
  * Avoid unnecessary modifiers.
  * Use specific examples; avoid vague language.
  * Be consistent.  (This guide is here to help!).
  * Do not use contractions as they cheapen the content and provide difficulty for readers of other languages.
  * Use the serial comma. (Also known as the Oxford comma, it helps clarify when items in a list of three, four, or more things are their own items.)
  * Do not use underline or all-uppercase capitalization for emphasis.
  * Avoid combining *italics* and **bold** (***except as last-resort extra emphasis***).
  * Skip unnecessary images and when including pictures or graphics provide alt text (image descriptions).
  * Lower barriers to taking action by ensuring links are descriptive and that in forms all fields have labels that are present and clear with a minimum of required fields.
  * Leave out irrelevent characterizations, especially when [writing about people](#writing-about-people).
  * When in doubt, read your writing out loud.

Many of these repeat or reinforce George Orwell's six rules from his essay "Politics and the English Language" (1946), and it is worth keeping all of them in mind, especially the last:

>  (i) Never use a metaphor, simile or other figure of speech which you are used to seeing in print.  
>  (ii) Never use a long word where a short one will do.  
>  (iii) If it is possible to cut a word out, always cut it out.  
>  (iv) Never use the passive where you can use the active.  
>  (v) Never use a foreign phrase, a scientific word, or a jargon word if you can think of an everyday English equivalent.  
>  (vi) Break any of these rules sooner than say anything barbarous.

[Much more on grammar and mechanics](#grammar-and-mechanics).

## Goals and Principles when writing

With every piece of content we publish, we aim to:

* **Empower**. Help people build the Open Web and the Free Software Movement by using language that informs them and encourages them to contribute.
* **Inspire**. Lift up our work, our clients' work, and our colleagues' work to inspire others to support them or pursue their own work.

In order to achieve those goals, we make sure our content is:

* **Authentic**. Write about what you are passionate about.
* **Useful**. Before you start writing, ask yourself: What purpose does this serve? Who is going to read it? What do they need to know?
* **Appropriate**. Write in a way that suits the situation. Just like you do in face-to-face conversations, adapt your tone depending on who you’re writing to and what you’re writing about.


## Voice and Tone

We strive to have the same voice all the time, but our tone changes to suit the situation.

### Voice

Agaric's voice is:

  * Straightforward and earnest.
  * Unafraid of bold, perhaps visionary, calls to action.
  * Irreverant; the human condition is too serious to take anything too seriously. 

### Tone

Agaric's tone is usually informal, but it is always more important to be clear than entertaining. When you are writing, consider the reader's state of mind. Are they curious about a post on our blog? Are they distrustful after being burned by a previous vendor? Are they excited to be engaging in a redesign? Once you have an idea of their emotional state, you can adjust your tone accordingly.

Agaric has a sense of humor, so feel free to be funny when it is appropriate and when it comes naturally to you. If in any doubt, do not make the joke. Generally avoid humor in written communication to clients.

#### Running gags

##### "The Agaric way"

We have been saying it from the beginning and are only half-serious about it.

## Writing about people

We write the same way we build software: with a person-first perspective.  Being aware of the impact of language is one way for us to live out our [values](values.md).

* Do not reference age or ability unless it is immediately relevant.
* Avoid gendered language and use the singular they.
* When writing about a person, use their preferred pronouns; if you do not know those, use their name.

Write for and about other people in a way that is compassionate, inclusive, and respectful.

### Age

Do not reference a person's age unless it is relevant to what you are writing. If it is relevant, include the person's specific age, offset by commas.
The CEO, 16, just got her driver's license.

Do not refer to people using age-related descriptors like "young", "old", or "elderly".

### Ability

Do not refer to a person's ability unless it is relevant to what you are writing. If you need to mention it, use language that emphasizes the person first: "she has a disability" rather than "she is disabled."  When writing about a person with disabilities, do not use the words "suffer," "victim," or "handicapped." "Handicapped parking" is OK.

Avoid ableist language.

### Gender and sexuality

Do not call groups of people "guys". "All" is a useful, non-gendered term for addressing groups of people.

Do not call women "girls".

Avoid gendered terms in favor of neutral alternatives, like "server” instead of “waitress” and “businessperson” instead of “businessman.”

Use "they" as a singular pronoun.

Use the following words as modifiers, but never as nouns:

* lesbian
* gay
* bisexual
* transgender (never "transgendered")
* trans
* queer
* LGBT

Do not use these words in reference to LGBTQIA people or communities:

* homosexual
* lifestyle
* preference

Do not use "same-sex" marriage, unless the distinction is relevant to what you are writing, simply use “marriage.”

When writing about a person, use their communicated pronouns. When in doubt, ask for their pronouns or use their name.

### Hearing

Use "deaf" as an adjective to describe a person with significant hearing loss. You can also use "partially deaf" or "hard of hearing".

### Medical conditions

Do not refer to a person's medical condition unless it is relevant to what you are writing.
If a reference to a person's medical condition is warranted, emphasize the person first. Do not call a person with a medical condition a *victim*. Instead, use *patient*.

### Mental and cognitive conditions

Do not refer to a person's mental or cognitive condition unless it is relevant to what you are writing. Never assume that someone has a medical, mental, or cognitive condition.
Do not describe a person as "mentally ill." If a reference to a person's mental or cognitive condition is warranted, use the same rules as writing about abilities or medical conditions and emphasize the person first.

### Vision

Use the adjective "blind" to describe a person who is unable to see. Use "low vision" to describe a person with limited sight.

## Grammar and mechanics

Adhering to certain rules of grammar and mechanics helps us keep our writing clear and consistent. This section will lay out our house style, which applies to all of our content unless otherwise noted in this guide.  (We cover a lot in this section— search if you are looking for something in particular.)

(grammar-and-mechancis-basics)=
### Basics

Write for all readers. Some people will read every word you write. Others will just skim. Help everyone read better by grouping related ideas together and using descriptive headers and subheaders.

Focus your message. Create a hierarchy of information. Lead with the main point or the most important content, in sentences, paragraphs, sections, and pages.

Be concise. Use short words and sentences. Avoid unnecessary modifiers.

Be specific. Avoid vague language. Cut the fluff.

Be consistent. Stick to the copy patterns and style points outlined in this guide.

### Guidelines

#### Abbreviations and acronyms

If there is a chance your reader will not recognize an abbreviation or acronym, spell it out the first time you mention it. Then use the short version for all other references. If the abbreviation is not clearly related to the full version, specify in parentheses.

First use: Network Operations Center  
Second use: NOC

First use: Coordinated Universal Time (UTC)  
Second use: UTC

If the abbreviation or acronym is well known to your full intended audience, like API or HTML in technical documentation, use it instead (and do not worry about spelling it out).


#### Active voice

Use active voice. Avoid passive voice.

In active voice, the subject of the sentence does the action. In passive voice, the subject of the sentence has the action done to it.

Yes: Marti logged into the account.  
No: The account was logged into by Marti.

Words like “was” and “by” may indicate that you’re writing in passive voice. Scan for these words and rework sentences where they appear.

One exception is when you want to specifically emphasize the action over the subject. In some cases, this is fine.

> Your account was flagged by our abuse team.

#### Capitalization

Sentence case capitalizes the first letter of the first word.  We use sentence case for headlines, subheads, and headings as well as every other sentence.  (We do **not** use title case, which capitalizes the first letter of every word except articles, prepositions, and conjunctions.  Instead, we let the formatting of titles carry the weight and use the more natural sentence case.)

When writing out an e-mail address or website URL, use all lowercase.

micky@agaric.coop  
agaric.com  

Do not capitalize words in the middle of sentences that are not proper nouns.  We do not capitalize internet, web, or e-mail.  For more, see the [word list](#word-list-specialized-vocabulary).


#### Contractions

They are bad! Even though they may give your writing an informal feel, they provide difficulty for readers that do not speak English. You can make your writing informal without the use of contractions.

#### Emoji

Keep out of blog posts and web copy entirely.
Emoji are a fun way to add humor and visual interest to your writing, but use them infrequently and deliberately.

#### Numbers

Spell out a number when it begins a sentence or is under ten. Ten to twenty is a judgement call (but usually spelled out if not paired with numerals). Otherwise, use the numeral. This includes ordinals, too.  
Ten new members started on Monday, and twelve start next week.  
I ate three doughnuts at coffee hour.  
Meg won first place in last year's hackathon.  
We hosted a group of eighth graders who are learning to code.  
We attended the 33rd annual picnic.  
One hundred trainees started the course but only 43 finished.

Numbers over 3 digits get commas:  
999  
1,000  
150,000  

Write out big numbers in full. Abbreviate them if there are space restraints, as in a microblog post or a chart: 1k, 150k.  

#### Dates

Generally, spell out the day of the week and the month.  Abbreviate only if space is an issue.   
Saturday, January 24  
Sat., Jan. 24  

#### Decimals and fractions

Spell out fractions.  
No: 2/3  
Yes: two-thirds  
Best: ⅔  
Use decimal points when a number can’t be easily written out as a fraction, like 1.375 or 47.2.  

#### Percentages

This depends on context— use the % symbol or spell out "percent" depending on which looks best. 

#### Ranges and spans

Use an en-dash (–­) to indicate a range or span of numbers.  (On Mac: Option + `-` (dash).  On Windows: Control + `-` (dash).  On Debian or Ubuntu with compose key enabled: compose + `--.` (dash dash period).  
It takes 20–30 days.  

#### Money

When writing about US currency, use the dollar sign before the amount. Include a decimal and number of cents if more than 0.  
$20  
$19.99  
When writing about other currencies, follow the same symbol-amount format:  
¥1  
€1  

#### Telephone numbers

Use dashes without spaces between numbers. Use a country code if your reader is in another country.  
555-867-5309  
+1-404-123-4567  

#### Temperature

Use the degree symbol and the capital F abbreviation for Fahrenheit.  
98°F  


#### Time

Use numerals and am or pm, with a space in between. Do not use minutes for on-the-hour time.  
7 am  
7:30 pm

Use an en-dash (–) between times to indicate a time period.  
7 am – 10:30 pm  

Specify time zones when writing about an event or something else people would need to schedule. Since Agaric was founded and has several worker-owners in Boston, Massachusetts, we default to ET.  
Abbreviate time zones within the continental United States as follows:  
Eastern time: ET  
Central time: CT  
Mountain time: MT  
Pacific time: PT  

When referring to international time zones, spell them out: Nepal Standard Time, Australian Eastern Time. If a time zone does not have a set name, use its Coordinated Universal Time (UTC) offset.  

Abbreviate decades when referring to those within the past 100 years.  
the 00s  
the 90s  

When referring to decades more than 100 years ago, be more specific:  
the 1650s  
the 1890s  
the 1910s

#### Punctuation

##### Apostrophes

The apostrophe's most common use is making a word possessive. If the word already ends in an s and it is singular, you also add an 's. If the word ends in an s and is plural, just add an apostrophe.  
The doughnut thief ate Sam's doughnut.  
The doughnut thief ate Chris's doughnut.  
The doughnut thief ate the managers' doughnuts.  

```{note}
Note the exception to this rule: the word **it**, which does not use an apostrophe for its possessive, for example: "Our competitor had all its clients' websites hacked."  Agaric avoids contractions so we do not ever use **it's** but rather **it is**.
```

Apostrophes can also be used to denote that you have dropped some letters from a word, usually to match spoken language— an unofficial contraction. This is fine when quoting or paraphrasing and giving the feel of a statement is important, but do it sparingly.  


##### Colons

Use a colon (rather than an ellipsis, em dash, or comma) to offset a list.  
Erin ordered three kinds of doughnuts: glazed, chocolate, and pumpkin.  
You can also use a colon to join 2 related phrases. If a complete sentence follows the colon, capitalize the 1st word.  
I was faced with a dilemma: I wanted a doughnut, but I’d just eaten a bagel.  

##### Commas

When writing a list, use the serial comma (also known as the Oxford comma).

Yes: David admires his parents, Oprah, and Justin Timberlake.  
No: David admires his parents, Oprah and Justin Timberlake.  

Otherwise, use common sense. If you are unsure, read the sentence out loud. Where you find yourself taking a breath, use a comma.  

##### Dashes and hyphens

Use a hyphen (-) without spaces on either side to link words into single phrase

  * first-time user
  * up-to-date software

To indicate a span or range, use an n-dash (–).

  * Monday–Friday

Use an em dash (—) with a space after the dash to offset an aside.  Use a true em dash, not hyphens (- or --).  If the set-off phrase has the main part of the sentence continuing, do not include spaces around the em dash.  If the set-off phrase ends the sentence, leave a space after the em dash.

  * We could build immensely powerful movements from the ground up, if we had a way to agree how shared resources of movements—including communication channels—would be controlled.
  * Migrate does almost all the work for us— we just need to create a Migration class and configure it using the constructor.


##### Ellipses

Ellipses (…) can be used to indicate that you are trailing off before the end of a thought. Use them sparingly. Do not use them for emphasis or drama, and do not use them in titles or headers.

  * "Where did all those doughnuts go?" Christy asked. Lain said, "I do not know…"

Ellipses, in brackets, can also be used to show that you are omitting words in a quote.

  * "When in the Course of human events it becomes necessary for one people to dissolve the political bands which have connected them with another and to assume among the powers of the earth, […] a decent respect to the opinions of mankind requires that they should declare the causes which impel them to the separation."

##### Periods

Periods go inside quotation marks. They go outside parentheses when the parenthetical is part of a larger sentence, and inside parentheses when the parenthetical stands alone.

  * Christy said, "I ate a doughnut."
  * I ate a doughnut (and I ate a bagel, too).
  * I ate a doughnut and a bagel. (The doughnut was Sam's.)

Use two spaces after a period between sentences.  This will be ignored in HTML but gives the opportunity to begin using a ligature that provides slightly more space at the end of a sentence than in acronyms, to prevent ambigities in interpretation such as:  "We went to the U.S. I told him."

##### Question marks

Question marks go inside quotation marks if they are part of the quote. Like periods, they go outside parentheses when the parenthetical is part of a larger sentence, and inside parentheses when the parenthetical stands alone.

##### Exclamation points

Use exclamation points sparingly, and never more than one at a time.

Exclamation points go inside quotation marks.  Like periods and question marks, they go outside parentheses when the parenthetical is part of a larger sentence, and inside parentheses when the parenthetical stands alone.

Never use exclamation points in failure messages or alerts. When in doubt, avoid!

##### Quotation marks

Use quotes to refer to words and letters, titles of short works (like articles and poems), and direct quotations.

Periods and commas go within quotation marks. Question marks within quotes follow logic— if the question mark is part of the quotation, it goes within. If you’re asking a question that ends with a quote, it goes outside the quote.

Use single quotation marks for quotes within quotes.

  * Who was it that said, “A fool and his doughnut are easily parted”?
  * Brad said, “A wise man once told me, ‘A fool and his doughnut are easily parted.’”  

##### Semicolons

Go easy on semicolons; they usually support long, complicated sentences that could be simplified.  Try an em dash (—) instead, or simply start a new sentence.

##### Ampersands

Do not use ampersands unless one is part of a company or brand name.  

  * Ben and Dan
  * Ben & Jerry’s
  * People, places, and things

##### File extensions

When referring generally to a file extension type, use all uppercase without a period. Add a lowercase s to make plural.

  * PDF
  * PNG
  * JPGs

When referring to a specific file, the filename should be lowercase:

  * agaric-example-org-website-proposal.pdf
  * micky-twitter-profile.jpg

```{seealso}
Agaric's [document versioning and filename conventions]().
```

##### Pronouns

If your subject's gender is unknown or irrelevant, use "they," "them," and "their" as a singular pronoun.  Use "he/him/his" and "she/her/her" pronouns as appropriate.  Do not use "one" as a pronoun.  

```{seealso}
For more on writing about gender, see [Writing about people](#writing-about-people). 
```

##### Quotations

When quoting someone in a blog post or other publication, use the past tense.  
“Working with Agaric has helped our business grow,” said Jamie Smith.  

##### Names and titles

The first time you mention a person in writing, refer to them by their first and last names. On all other mentions, refer to them by their first name.

Capitalize the names of departments and teams (but not the word "team" or "department").  
Marketing team  
Support department  
Capitalize individual job titles when referencing to a specific role. Do not capitalize when referring to the role in general terms.  
Our new Marketing Manager starts today.  
All the managers ate doughnuts.  
Do not refer to someone as a “guru,” “rockstar,” or “wizard” unless they literally are one.  

**Schools**  
The first time you mention a school, college, or university in a piece of writing, refer to it by its full official name. On all other mentions, use its more common abbreviation.  
Georgia Institute of Technology, Georgia Tech  
Georgia State University, GSU  
States, cities, and countries   
Spell out all city and state names. Do not abbreviate city names.  
Per AP Style, all cities should be accompanied by their state, with the exception of: Atlanta, Baltimore, Boston, Chicago, Cincinnati, Cleveland, Dallas, Denver, Detroit, Honolulu, Houston, Indianapolis, Las Vegas, Los Angeles, Miami, Milwaukee, Minneapolis, New Orleans, New York, Oklahoma City, Philadelphia, Phoenix, Pittsburgh, St. Louis, Salt Lake City, San Antonio, San Diego, San Francisco, Seattle, Washington.  
On first mention, write out United States. On subsequent mentions, US is fine. The same rule applies to any other country or federation with a common abbreviation (European Union, EU; United Kingdom, UK).  

**URLs and websites**
Capitalize the names of websites and web publications. Do not italicize.  
Avoid spelling out URLs, but when you need to, leave off the https://www.  

## Writing about Agaric  

Our company's legal entity name is **Agaric, LLC**. Our trade name is **Agaric**. Use **Agaric, LLC** only when writing legal documents or contracts. Otherwise, use **Agaric** or **Agaric Technology Collective**.

Always capitalize Agaric.

Refer to Agaric as *we*, not *it*.  

Capitalize the proper names of Agaric platforms and projects.   

## Writing about other companies

Honor companies' own names for themselves and their products. Go by whatever is used on their official website. Unless they end with an exclamation point ('!'); that is absurd and will not be respected!

Refer to a company or product as *it* (not *they*).  

## Slang and jargon

Write in plain English. If you need to use a technical term, briefly define it so everyone can understand.

## Text formatting

Use italics to indicate the title of a long work (like a book, movie, or album) or to emphasize a word.  
Use italics when citing an example of an element, or referencing button and navigation labels in step-by-step instructions:  

> When you are done, click *Send*.  

Do not use underline formatting, which typically indicates a link, and do not use a combination of italic, bold, or all capital letters.  

## Write positively

Use positive language rather than negative language. One way to detect negative language is to look for words like "cannot" or "do not" (or the contractions we want to remove anyway, "can't" and "don't").  
Yes: To get a doughnut, go to the kitchen.  
No: You can't get a doughnut if you don't go to the kitchen.  


## Content Types

Content at Agaric takes many forms. Here is a rundown of the types of content we most often write.

### Short

#### Interface copy  

What: Explanatory messaging that guides and informs users  
Length: 10-50 words  
Example: To get started, set a URL to import.  

##### Success message
What: Short, encouraging message letting the user know they’ve accomplished something in the app  
Length: 5-20 words  
Example: Excellent! Your account has been created 

##### Error or failure message
What: Short message that alerts the user to a problem in their account or on their site 
Length: 20-75 words  
Example: Looks like you may have left our default header content unmodified. Specifically, we still see ‘Use this area to offer a short preview’ in the pre-header/header area.  


#### Social media

What: Posts on Twitter, Facebook, Instagram, and LinkedIn that highlight blog posts, events, notable Agaric clients, and more  
Length: 20-30 words  
Owner: Marketing, support  
Example: The City of Camrbidge uses Agaric's Find It platform to make it easier for people to find activities, events, and resources. Here is how it works: https://agaric.coop/work/find-it-cambridge

```{seealso}
[Writing for social media](#writing-for-social-media)
```

### Long

#### Blog post  
What: Informative articles about Agaric work, initiatives, and announcements  
Length: 400-800 words  
Example: Flat Comments in Drupal 8 without Dangerous Secret Threading

```{seealso}
[Writing blog posts](#writing-blog-posts)
```


#### E-mail newsletter

What: Email campaigns that market our services/products and inform or empower our users  
Length: 200-1000 words  
Example: Agaric needs to finally start one!

```{seealso}
[Writing newsletters](#writing-newsletters)
```

#### How To article

What: Easily digestible content that walks users through a process or problem  
Length: 300-1,000 words  
Home: Blog  
Example: Getting Started with Lists  

```{seealso}
[Writing technical content](#writing-technical-content)
```

#### Legal content  

What: Policies that explain how we protect user privacy and how we handle accounts  
Length: 1,000-4,000 words  
Example: Terms of Use

#### Press release

What: Quick, informative announcements that we send to our media list and post to our Press Releases page.  
Length: 300-500 words  
Example: Agaric Launches Data Visualization Platform for Healthcare Providers 


## Web Elements

Every piece of content we publish is supported by a number of smaller pieces. This section lays out our style in regards to these web elements, and explains our approach to the tricky art of SEO.

(web-elements-guidelines)=
### Guidelines

#### Alt text

Alt text is a way to label images, and it is especially important for people who can’t see the images on our website. Alt text should describe the image in a brief sentence or two.
For more on how and why we use alt text, read the [Accessibility section](#writing-for-accessibility).

#### Buttons

Buttons should always contain actions. The language should be clear and concise. Capitalize every word, including articles. It is OK to use an ampersand in button copy.
Standard website buttons include:

  * Log In
  * Sign Up Free
  * Subscribe
  * Email Us

#### Checkboxes

Use sentence case for checkboxes.

#### Drop-down menus

Use title case for menu names and sentence case for menu items.

#### Forms

Form titles should clearly and quickly explain the purpose of the form.

Use title case for form titles and sentence case for form fields.

Keep forms as short as possible.

Only request information that we need and intend to use. Don’t ask for information that could be considered private or personal, including gender. If you need to ask for gender, provide a field the user can fill in on their own, not a drop-down menu.  (Or use a comprehensive solution like the [Drupal gender field module](https://www.drupal.org/project/gender).)

#### Headings and subheadings

Headings and subheadings organize content for readers. Be generous and descriptive.  
Headings (H1) give people a taste of what they’re about to read. Use them for page and blog titles.  
Subheadings (H2, H3, etc.) break articles into smaller, more specific sections. They give readers avenues into your content and make it more scannable.  
Headings and subheadings should be organized in a hierarchy, with heading first, followed by subheadings in order. (An H2 will nestle under H1, an H3 under H2, and on down.)  
Include the most relevant keywords in your headings and subheadings, and make sure you cover the main point of the content.  
Use title case, unless the heading is a punctuated sentence. If the heading is a punctuated sentence, use sentence case. Use sentence case for subheadings regardless of end punctuation.

#### Links

Provide a link when you refer to anything web-accessible that is relevant and hosted by a trusted external resources.

Do not include preceding articles (a, an, the, our) when you link text. For example:  
Yes: Read the [content style guide](content-style-guide.md#links) for details.  
No: Read [the content style guide](content-style-guide.md#links) for details.  

If a link comes at the end of a sentence or before a comma, do not link the punctuation mark.

Do not say things like "Click here!" or "Click for more information" or "Read this."  Write the sentence as you normally would, and link relevant keywords.

```{seealso}
[Baseline Styleguide: Links](making-websites/baseline-styleguide.md#links)
```

#### Lists

Use lists to present steps, groups, or sets of information. Give context for the list with a brief introduction. Number lists when the order is important, like when you are describing steps of a process. Do not use numbers when the list's order does not matter.

If one of the list items is a complete sentence, use proper punctuation and capitalization on all of the items. If list items are not complete sentences, do not use punctuation, but do capitalize the first word of each item.

#### Navigation

Use title case for main or global navigation. Use sentence case for subnavigation.

Navigation links should be clear and concise.


#### Radio Buttons

Use title case for headings and sentence case for button fields.

#### Related articles

Sometimes a long piece of copy lends itself to a list of related links at the end. Don’t go overboard—4 is usually plenty.

Related articles should appear in a logical order, following the step down/step up rule: The first article should be a step down in complexity from the current article. The second one should be a step up in complexity to a more advanced article.

If you can, avoid repeating links from the body text in related articles.

#### Titles

Titles organize pages and guide readers. A title appears at the beginning of a page or section and briefly describes the content that follows.  
Titles are (you guessed it) in title case.  
Don’t use punctuation in a title unless the title is a question.  

#### SEO

We write for humans, not machines. We do not use gross SEO techniques like keyword stuffing to bump search results. But we also want to make it easy for people and search engines to find and share our content. Here are some good ways to do this:

Organize your page around one topic. Use clear, descriptive terms in titles and headings that relate to the topic at hand.  
Use descriptive headings to structure your page and highlight important information.
Give every image descriptive alt text.  


## Writing blog posts

Agaric blog posts are written by people from all over the company, not just those with “writer” in their job titles. We love having people who know the most about what they do blog about their work. The person most familiar with the subject is in the best position to convey it, and other Agarics can help with brainstorming and editing as needed.

(writing-blog-posts-basics)=
### Basics

We update the main Agaric blog a couple times every month. We generally publish:

  * Elaborations on Agaric case studies
  * Things we have learned that may be of interest to other web developers
  * Informative or call-to-action posts about topics at the intersection of libre software, cooperation, and human liberation

We publish blog posts that explain the “why” behind the work we do at Agaric. We want to show people that we are an industry leader and we use our blog to tell the stories behind our work and services.


### Guidelines

When writing for the blog, follow the style points outlined in the Voice and tone and Grammar and mechanics sections. Here are some more general pointers, too.

#### Be casual, but smart

This is not a term paper, so there is no need to be stuffy. Drop some knowledge while casually engaging your readers with conversational language.

#### Be specific

If you are writing about data, put the numbers in context. If you are writing about an Agaric client, give the reader plenty of information about the organization’s purpose, workflow, technical needs and results.

#### Get to the point

Get to the important stuff right away, and don’t bury the kicker. Blog posts should be scannable and easy to digest. Break up your paragraphs into short chunks of three or four sentences, and use subheads. Our users are busy, and we should always keep that in mind.

#### Link it up

Feel free to link away from Agaric if it helps you explain something.

#### Make them laugh

Agaric is a fun company, and we want our blog to reflect this. Feel free to throw in a joke here and there, or link out to a funny GIF or YouTube video when appropriate. Just do not overdo it or force it.

#### Use tags and keywords

In Drupal, add keywords that apply to your article. Look through existing posts for common tags. If you are not sure if a word should be a tag, it probably should not be a tag.

#### Use pictures

Include images in your blog posts when it makes sense. If you’re explaining how to do something, include screenshots to illustrate your point. Make sure to use alt text.


## Writing case studies

*This section is heavily influenced by Nick Disabato of [Draft](https://draft.nu).*

A case study shows potential clients a named real-world client we have worked with, along with the impact (economic or other) we had on their business or organization, with social proof from one or more members of the client’s team.

### What to enforce upfront

This involves three bits that need to be contractually enforced before starting any engagement:

* The client must be explicitly named. 
* The impact must be measurable. We cite a specific number that indicates the impact of our work. No numeric ranges here: only specific, precise numbers.
* Social proof must be present. Get an awesome quotation about the impact of Agaric’s work from the client and publish the case study with their name in it.
* These must be contractually enforced before the engagement begins, because they are always points of contention later.

### Components of a Great Case Study

**The problem.** Explain or enumerate why a client came to Agaric, as well as any unique aspects of their problem.  (The more expensive a problem is for a client, the more clearly we can show our impact.)

**Our thinking.** How did we begin approaching the problem? What research did we conduct to address it? What worked and what did not? What did we try?

**The outcome.** What was the measurable impact of our work?

**A quotation.** What did the client say about our work?

**What happened next.** Did the engagement expand in any capacity? Did we do other valuable things for them that were not part of the initial, agreed-upon scope?

**A call to action.** Case studies exist to generate additional leads for the consultancy, so end with a clear next step for the reader to take – one that probably begins with introducing the kind of work we can do for them, and encourages them to apply.


## Writing about initiatives 

Initiatives are meant to inform our audiences of how our work extends out to the greater world. Clearly convey their purpose, impact and how people can get involved.


## Writing about upcoming events

Start with the essentials: what, when, where, cost and then follow with more specific details such as information about the people presenting or links to further background information.


## Writing profile pages

This is an opportunity for people’s individuality to come through. Make these personable, highlight people’s passions, expertise and interesting facts that connect them to the reader.


## Writing technical content

Someone reading technical content is usually looking to answer a specific question. That question might be broad or narrowly-focused, but either way our goal is to provide answers without distraction.

For each project, consider your audience’s background, goal, and current mood. Ask these questions:

* Is the reader a prospective user, a new user, or an experienced user? 
* What is the goal of the reader? To complete a task? To research a topic?
* Is the reader in the middle of a task? Are they in a hurry? Could they be frustrated?

We don’t want to overload a reader with unnecessary information, choices to make, or complex ideas or phrases, when we don’t have to. This is particularly critical when a user may be new and/or frustrated. When relevant, prime the reader with a brief outline of an article’s focus in an introductory paragraph or section, and stick to the topic at hand. Keep sentences, paragraphs, and procedural steps focused and concise.


### Drafting technical content

Before you begin writing a new article, reach out to a subject matter expert (like an engineer, tester, designer, researcher, or technical support advisor) to get as much information as possible.  You may only use a small portion of what you learn, but it helps to have more information than you need to decide where to focus your article.

Consider how many articles are needed and what article types will best describe a new feature or tasks to the user.

Outline your article, then write a draft.  Stay in touch with your subject matter expert and revise as needed for accuracy, consistency, and length.

When you are happy with a draft, pass it to another technical writer for peer review. Then show it to a lead technical writer for additional review and revisions.  For new content or highly complex content, send last draft to your subject matter expert for final approval.


### Writing technical content

When writing technical content, follow the style points outlined in the Voice and tone and Grammar and mechanics sections.  Here are some more general pointers, too.

#### Stay relevant to the title

When a user clicks the title of an article, they expect to find the answer they want.  Do not stray too far from the title or topic at hand.  Use links to make related content available.  If you find you are getting too far from the intended topic, then you may need to create a separate but related article.

#### Keep headlines and paragraphs short and scannable

Focused users often scan an article for the part that will answer their particular question. Be sure headlines are short, descriptive, and parallel, to facilitate scanning.

#### Use second-person and describe actions to a user

Technical content talks to users when support agents cannot.

#### Strive for simplicity and clarity

Be as clear as possible. Use simple words and phrases, avoid gerunds and hard-to-translate idioms or words, focus on the specific task, limit the number of sentences per paragraph. If you must include edge cases or tangentially related information, set it aside in a Before You Start list or Notes field.

#### Provide context through embedded screenshots and GIFs

Screenshots and GIFs may not be necessary for every article or process, but can be helpful to orient new users. Crop screenshots tightly around the action to focus attention.

### Editing technical content

We edit technical content based on three goals:
* Digestibility  
  * Cut or tighten redundancies, gerunds, adverbs, and passive constructions.  
  * Use the simplest word.  
  * Limit paragraphs to three sentences.  
* Consistency  
  * Use the labels and terminology used in the Agaric client's app.  
  * Use specific, active verbs for certain tasks.  
  * Choose basic words and phrases to facilitate consistency across translated content.  
* Helpfulness  
  * Stay conversational, without using contractions.  
  * Avoid qualifiers that muddy meaning.  
  * Craft clear transitions from section to section to orient the reader.  

### Formatting technical content

Technical content uses organization, capitalization, and other formatting to help convey meaning. Although different articles are organized differently, some formatting tips are consistent throughout all technical content.

#### Capitalization

Capitalize proper names of Agaric products, features, pages, tools, and team names when directly mentioned. In step-by-step instructions, capitalize and italicize navigation and button labels as they appear in the app.

#### Headings

Group article content with H2s and H3s. Use H2s to organize content by higher-level topics or goals, and use H3s within each section to separate supporting information or tasks.  

#### Ordered Lists

Only use ordered lists for step-by-step instructions. Separate steps into logical chunks, with no more than 2 related actions per step. When additional explanation or a screenshot is necessary, use a line break inside the list item.

#### Unordered Lists

Use unordered lists to display examples, or multiple notes in a Notes block. If an unordered list comprises more than 10 items, use a table instead.

## Writing newsletters

An e-mail newsletter is a periodic publication giving insights and updates on a topic— in our case, Agaric generally.

Only send when we have something to say.

```{note}
People should be able to keep up with Agaric however they choose, so we should introduce the ability to subscribe to blog posts by e-mail as well as by RSS.  But that is different from an e-mail newsletter.
```

### Guidelines

E-mail newsletters use our [voice and tone](#voice-and-tone) and follow our [grammar and mechanics](#grammar-and-mechanics), in addition to these newsletter-specific considerations.

### Consider all elements

Every email newsletter is made up of the following elements. Make sure they’re all in place before clicking send:

#### From name

This is usually the company or team's name. It identifies the sender in the recipient's inbox.

#### Subject line

Keep your subject line descriptive. There is no perfect length, but some email clients display only the first words. Tell—don't sell—what's inside. Subject lines should be in sentence case. (Note that this is different from a headline, which you may want to include in the campaign itself.)

#### Preheader text

The top line of your e-mail appears beside each subject line in most inboxes. Provide the info readers need when they're deciding if they should open.

#### Body copy

Keep your content concise. Write with a clear purpose, and connect each paragraph to your main idea. Add images when they are helpful.

#### Call to action

Make the next step clear. Whether you are asking people to buy something, read something, share something, or respond to something, offer a clear direction to close your message so readers know what to do next.

#### Footer

All campaigns follow CAN-SPAM rules.  Include an unsubscribe link, mailing address, and permission reminder in the footer of each newsletter.

### Consider your perspective

When sending an e-mail newsletter from Agaric, use the third person "we." When sending a newsletter as an individual, use the first person.

### Use a hierarchy

Most readers will be scanning your emails or viewing them on a small screen. Put the most important information first.

### Include a call to action

Make the reader's next step obvious, and close each campaign with a call to action. Link to a blog post, event registration, purchase page, or signup page. You can add a button or include a text link in the closing paragraph.

### Avoid unnecessary links

More than 50 percent of emails are read on a mobile device. Limit links to the most important resources to focus your call to action and prevent errant taps on smaller screens.

### Use alt text

Some email clients disable images by default. Include an alt tag to describe the information in the image for people who are not able to see it.

### Segment your audience

It is exciting to send to thousands of people at once, but it is doubtful that every subscriber is interested in every topic. Segment your list to find a particular audience that is likely to react.

Once you have selected an audience, adjust the language to fit their needs. For example, people who have attended trainings are more likely to understand and appreciate direct, technical terms.

### Test your campaigns

Use the preview mode to begin, and run an Inbox Inspection to see your newsletter in different email clients. Read your campaign out loud to yourself, then send a test to a coworker for a second look.

## Writing for social media

We use social media to build relationships with Agaric clients and supporters and share all the cool stuff we do. But it also creates lots of chances to say the wrong thing, to put off clients, and to make people think less of us. So we are careful and deliberate in what we post to our social channels. This section lays out how we strike that delicate balance.

### Basics

Agaric has a presence on most major social media platforms. Here are our most active accounts and what we usually post on each:

* [Twitter](https://twitter.com/agaric): Collective news, events, paid trainings, media mentions, evergreen content, calls for collaborators.
* [Fediverse/Mastodon](https://social.coop/@agaric): Collective news, events, paid trainings, media mentions, evergreen content, calls for collaborators.
* [Facebook](https://www.facebook.com/agaric.collective/): Collective news, events, paid trainings, media mentions, evergreen content, calls for collaborators.
* [LinkedIn](https://www.linkedin.com/company/agaric/): Collective news, events, paid trainings, media mentions, evergreen content, calls for collaborators.

### Guidelines

Our writing for social media should generally follow the style points outlined in the [Voice and tone](#voice-and-tone) and [Grammar and mechanics](#grammar-and-mechanics) sections. Here are some additional pointers.

#### Write concisely, but clearly

Some social media platforms have a character limit; others do not.  But for the most part, we keep our social media copy short.

  * Twitter: 280 characters.
  * Mastodon by social.coop: 500 characters.
  * Facebook: No limit, but aim for 1–3 sentences.
  * LinkedIn: No limit, longer posts OK.

To keep your posts short, simplify your ideas or reduce the amount of information you are sharing—but not by altering the spelling or punctuation of the words themselves. It is fine to use the shorter version of some words, like "info" for "information". But do not use numbers and letters in place of words (no "4" instead of "for" or "u" for "you".

Threads are fine to express more complex ideas or more related information, but ensure each post in the thread stands on its own as a coherent statement.

### Engagement

Do your best to adhere to Agaric style guidelines when you are using our social media channels to correspond with users. Use correct grammar and punctuation—and avoid excessive exclamation points.

When appropriate, you can tag the subject of your post on Twitter or Facebook. But avoid directly tweeting at or otherwise publicly tagging a post subject with messages like, "Hey, we wrote about you!" Do not ask for retweets, likes, or favorites.

  * Yes: “We talked with @dinarcon about configuring YAML definition files https://agaric.coop/blog/drupal-migrations-reference-list-configuration-options-yaml-definition-files”
  * No: “Hey @mlncn, can you RT this post we wrote about the training you are giving? https://agaric.coop/blog/learn-how-migrate-drupal-8-successfully-nonprofit-technology-pre-conference-day”

Do respond promptly to people who tag us (and are not trolls).

### Hashtags

We employ hashtags rarely and deliberately. We may use them to promote an event or connect with users at a conference. Do not use current event or trending hashtags to promote Agaric unless it applies directly to our work (eg: #drupal #freesoftware #netneutrality).

### Trending topics

* Do not use social media to comment on trending topics or current events that are unrelated to Agaric.
* Be aware of what is going on in the news whenever you are publishing social content for Agaric.
* During major breaking news events, turn off all scheduled social posts.

## Writing for Accessibility

We are always working to make our content more accessible and usable to the widest possible audience. Writing for accessibility goes way beyond making everything on the page available as text. It also affects the way you organize content and guide readers through a page. Depending on the audience and country, there may be laws governing the level of accessibility required. At minimum, an accessible version should be available. Accessibility includes users of all mental and physical capacities, whether situational (broken glasses, circling helicopters) or more permanent.

(accessibility-basics)=
### Basics

We write for a diverse audience of readers who all interact with our content in different ways. We aim to make our content accessible to anyone using a screen reader, keyboard navigation, or Braille interface, and to users of all cognitive capabilities.

As you write, consider the following:

* Would this language make sense to someone who does not work here?  
* Could someone quickly scan this document and understand the material?  
* If someone cannot see the colors, images, or video, is the message still clear?  
* Is the markup clean and structured?  
* Mobile devices with accessibility features are increasingly becoming core communication tools, does this work well on them?  

Many of the best practices for writing for accessibility echo those for writing technical content, with the added complexity of markup, syntax, and structure.

 * Some people will read every word you write.  Others will scan.  Help everybody by grouping related ideas together and by using descriptive headers and subheaders.
 * Create a hierarchy, with the most important information first.
Place similar topics in the same paragraph, and clearly separate different topics with headings.
 * Use plain language. Write short sentences and familiar words.
 * Links should provide information on the associated action or destination.  Avoid writing "click here" or "learn more."
 * Avoid using images when descriptive text will do.
 * Avoid directional instructions or language that requires the reader to see the layout or design of the page.
 * Label inputs on forms with clear names and use appropriate tags. Think carefully about what fields are necessary, and especially which ones you mark as required.

(accessibility-guidelines)=
### Guidelines

* Avoid directional language  
* Avoid directional instructions and any language that requires the reader to see the layout or design of the page. Avoiding this is helpful for many reasons, including having the instructions still make sense after layout changes on mobile.  

Yes: "Select from these options" (with the steps listed after the title)  
No: "Select from the options in the right sidebar."  

### Use a skip navigation link

Use a skip navigation link so that screen reader or keyboard-only users can avoid listening to or tabbing through many navigation elements before getting to the main content.  Drupal provides this by default.

### Use headers

Headers should always be nested and consecutive. Never skip a header level for styling reasons. To help group sections, be sure the page title is H1, top-level sections are H2s, and subsequent inside those are H3 and beyond. Avoid excessive nesting.

### Employ a hierarchy

Put the most important information first. Place similar topics in the same paragraph, and clearly separate different topics with headings.
Starting with a simple outline that includes key messages can help you create a hierarchy and organize your ideas in a logical way. This improves scannability and encourages better understanding.
Make true lists instead of using a paragraph or line breaks.

### Label forms

Label inputs with clear names, and use appropriate tags. Think carefully about what fields are necessary, and especially which ones you mark as required. Label required fields clearly. The shorter the form, the better.

### Use descriptive links

Links should provide information on the associated action or destination. Try to avoid “click here” or “learn more.”

### Use plain language

Write short sentences and use familiar words. Avoid jargon and slang. If you need to use an abbreviation or acronym that people may not understand, explain what it means on first reference.

### Use alternative text

The alt tag is the most basic form of image description, and it should be included on all images.  The wording will depend on the purpose of the image:  

* If it is a creative photo or supports a story, describe the image in detail in a brief caption.  
* If the image is serving a specific function, describe what is inside the image in detail. People who do not see the image should come away with the same information as if they had.  
* If you are sharing a chart or graph, include the data in the alt text so people have all the important information.  

Supplement images with standard captions when you realize .

### Make sure closed captioning is available

Closed captioning or transcripts should be available for all videos. The information presented in videos should also be available in other formats.

### Be mindful of visual elements

Aim for high contrast between your font and background colors. Tools in the resources section should help with picking accessible colors.

Images should not be the only method of communication, because images may not load or may not be seen. Avoid using images when the same information could be communicated in writing.

(accessibility-resources)=
### Resources

* [Accessibility evaluation for web writers](http://www.4syllables.com.au/2013/05/writers-accessibility-evaluation/)  
* [Accessibility cheatsheet](http://bitsofco.de/2015/the-accessibility-cheatsheet/)  
* [WAVE Web Accessibility Evaluation Tool](http://wave.webaim.org/)  


## Writing for Translation

* Use active voice.
* Avoid double negatives.
* Do not use contractions as they cheapen the content and provide difficulty for readers that do not speak English of other languages.
* Avoid using synonyms for the same word in a single piece of writing.
* Write briefly, but do not sacrifice clarity for brevity. You may need to repeat or add words to make the meaning of your sentences clear to a translator.
* Avoid slang, idioms, and cliches.
* Avoid unnecessary abbreviations.

Agaric serves users in several countries and territories, not just the United States. As our user base grows, it becomes more and more important that our content is accessible to people around the world.

We call the process of writing copy for translation "internationalization." This section will address things you can do to help international audiences, including translators, better comprehend your text.

(writing-for-translation-basics)=
### Basics

We try to write all of our content in standard, straightforward English that can be understood by users with limited English proficiency. It is much easier for a translator to clearly communicate ideas written in straightforward, uncomplicated sentences.

Here are some guiding principles for writing for international audiences:

Use active voice. We always aim for this, but it is especially important when writing for translation.

Use the subject-verb-object sentence structure. It is not used by all languages, but it is widely recognized. That does not mean we should rely on it.

Use positive words when talking about positive situations. For example, because a question like "Don't you think she did a great job?" begins with a negative word, a non-native English speaker may interpret its implication as negative. A better version would be "She did a great job, yes?"

Avoid contractions.

(writing-for-translation-guidelines)=
### Guidelines

When writing for international audiences, we generally follow what is outlined in the Voice and tone and [Grammar and mechanics](#grammar-and-mechanics) sections. But in this section more than others, some style points contradict what is stated elsewhere in the guide. If you are writing something to be translated, the guidelines in this section should take precedence.

#### Consider cultural differences

Agaric's voice is conversational and informal. However, in some cultures, informal text may be considered offensive. Check with your translator to see if this is the case for the particular language you're writing for.

Some languages have a clearer distinction between a formal or informal tone. (For example, in Spanish, it is possible to write informally where tú = you or formally where usted = you.)

When writing text that will be translated, be careful about making references to things of local or regional importance. These may not be recognizable to readers outside the US.

#### Prioritize clarity

Keep your copy brief, but do n’t sacrifice clarity for brevity. You may need to repeat or add words to make the meaning of your sentences clear to a translator.

#### Repeat verbs that have multiple subjects.

Yes: Customers who have ordered online can pick up their food at the cashier. Walk-in customers should stop by the cashier to order their food.  
No: Customers who have ordered online or who are walk-ins should stop at the cashier to order or pick up their food.  

#### Repeat helping verbs belonging to multiple verbs

Yes: Agaric can build your website or can train your development team to build more powerful websites.  
No: Agaric can build your website or train your development team to build more powerful websites.

#### Repeat subjects and verbs

Yes: Most Agarics have used contractions, but Micky has not. 
No: Most Agarics have used contractions, but not Micky.  

#### Repeat markers in a list or series

Yes: Use Agaric's Find It platform to post opportunities, to list service providers, and to connect kids with activities. 

No: Use Agaric's Find It platform to post opportunities, list service providers, and connect kids with activities.

#### Leave in words like "then", "a", "the", "to", and "that", even if you think they could be cut  

Yes: If there is not a test site set up, then you will need to create a test site before you can safely evaluate functionality changes.  
No: If there is not a test site set up, you will need to create a test site before you can safely evaluate functionality changes.

Yes: Be sure that you are truly ready to show your work to the world before pushing your changes to live.  
No: Be sure you are truly ready to show your work to the world before pushing your changes to live.


#### Avoid ambiguity and confusion

Many words, parts of speech, and grammar mechanics we don’t think twice about have the potential to cause confusion for translators and non-native English speakers. Here are some of the big trouble spots to avoid.

#### Avoid unclear pronoun references

Yes: Many believe that making functionality changes directly to a live site is OK. Such action can actually cause a site to break. Making functionality changes directly to a live site is not recommended.  
No: Many believe that making functionality changes directly to a live site is OK. This can cause a site to break. It is not recommended.

#### Avoid -ing words

In English, many different types of words end in -ing: nouns, adjectives, progressive verbs, etc. But a translator who is a non-native English speaker may not be able to recognize the distinctions and may try to translate them all in the same way.

Because of this, we want to avoid -ing words when possible. One exception to this rule is words like “graphing calculator” and “riding lawnmower,” where the -ing word is part of a noun's name.

Here are some other cases where you might see -ing words, and suggestions for how to edit around them.

##### Gerunds

Yes: In this article we will talk about how to encourage participants to sign up for training.  
No: In this article we will talk about getting training participants.  

##### Adjectives

Yes: At the top of the page, there is Ben with a smile on his face.  
No: At the top of the page, there is a smiling Ben.  

##### Parts of verbs

Yes: Several developers are currently working on that feature.  
No: Several developers are working on that feature. (When you can’t easily avoid the -ing word, it may help to add an adverb to clarify the meaning.)  

##### Parts of phrases modifying nouns

Yes: From our backyard, we could hear the planes that took off from the airport.  
No: From our backyard, we could hear the planes taking off from the airport.  

#### Other words and mechanics to avoid

* Slang, idioms, and cliches  
* Contractions (English contractions may be harder to translate)  
* Shortened words, even if they are common in English (use "application," not "app")  
* Uncommon foreign words (use "genuine," not "bona fide")  
* Unnecessary abbreviations (use "for example," not "e.g.")  
* Verbing nouns (or otherwise converting one part of speech into another) if the usage is not common (use "Send us an e-mail" instead of "message us")  
* Non-standard or indirect verb usage (use "he said," not "he was like" or "he was all")  
* Double negatives  
* Using multiple different words for the same thing in a single piece of writing. Instead of mixing it up with "campaign," "newsletter," "bulletin," etc., pick one term and stick with it.

#### Beware words with multiple meanings

"Once" (could mean "one time," "after," "in the past," or "when")  
No: Once you log in, you will see your account's dashboard.  
Yes: After you log in, you will see your account's dashboard.  

"Right" (could mean "correct," "the opposite of left," or "reactionary")  
No: Click the right image and drag it to the right pane.  
Yes: Click the correct image and drag it to the pane at right.  

"Since" (could refer to a point in time, or a synonym of "because")  
No: Since you already registered, this information will be filled out for you.  
Yes: Because you already registered, this information will be filled out for you.  

"Require to" (could confuse the relationship between subject and object)  
No: A registered account is required to post to the forum. (This could imply that people  with paid accounts are required to send autoresponders.)  
Yes: An account 

“Has” or “have” plus past participle (could confuse the relationship between subject and object)  
Yes: The folder contains sent campaigns.  
No: The folder has sent campaigns.  

#### Numbers

**Measurements**
When writing for an international audience, use the metric system. Spell out all units and avoid abbreviation.

**Currency**
Many countries call their currency "the dollar," but the value is going to differ between countries. The US dollar is not the same as the Canadian dollar, for example. So it is important to specify.

Avoid colloquial phrases that relate to money, like "five-and-dime," "greenbacks," or "c-notes." These will not translate well.

## Word list (specialized vocabulary)

  * add-on (noun, adjective), add on (verb)  
  * back end (noun), back-end (adjective)  
  * beta  
  * checkbox  
  * co-op
  * cooperative
  * coworker
  * Cloudflare
  * COVID-19
  * dropdown (noun, adjective), drop down (verb)
  * e-commerce
  * e-mail
  * ePub
  * e-mail (always hyphenate, never capitalize unless it begins a sentence)
  * emoji (singular and plural)
  * front end (noun), front-end (adjective)
  * geolocation
  * hashtag
  * homepage
  * internet (only capitalize unless if it begins a sentence)
  * login (noun, adjective), log in (verb)
  * Like (the social media activity)
  * May First Movement Technology, or May First on subsequent uses (not MayFirst or MFPL or the old name, May First / People Link)
  * Nextcloud
  * NOVA Web Development (abbreviated NWD)
  * OK
  * online (never capitalize unless it begins a sentence)
  * opt-in (noun, adjective) , opt in (verb)
  * pop-up (noun, adjective), pop up (verb)
  * signup (noun, adjective), sign up (verb)
  * sync
  * tweet, retweet
  * username
  * URL
  * web hosting
  * website
  * WiFi

## Words to avoid

  * automagically
  * funnel, incentivize, leverage, disruption, thought leader, or other fluffy corporate terms
  * internets, interwebs, or any other variation of the word "internet"
  * ninja, rockstar, wizard, unicorn (unless referring to a literal ninja, rockstar, wizard, or unicorn)
  * young, old, elderly, or any other word describing a person's age
  * crushing it, killing it
  * crazy, insane, or similar words to describe people

## Styleguide credit

Originally adapted from [Mailchimp's content style guide](https://styleguide.mailchimp.com/), Agaric's content style guide is likewise available under a [Creative Commons Attribution-NonCommercial 4.0 International license](http://creativecommons.org/licenses/by-nc/4.0/).

```{seealso}

  * [Marketing documentation](marketing).
  * Information about [copyrights and trademarks](copyright-and-trademarks).
  * [Baseline Styleguide](making-websites/baseline-styleguide.md)
  * [Agaric website basics](agaric-website/basics) and [content entry on Agaric's sites](agaric-website/agaric-site-content-entry).
```
