# Baseline Styleguide

A styleguide with no style of its own, but nevertheless providing the basic requirements any look-and-feel guidelines must conform to.

```{seealso}
For writing (rather than presentation), see [Agaric's Content Style Guide](/content-style-guide.md).
```

## Links

Links should look different than regular text, strong text, or emphasized text. They should have a hover state that communicates they are interactive, and should have a distinct active and visited state. When setting the hover state of links, be sure to include focus state as well, to help readers using assistive technologies and touch devices.
