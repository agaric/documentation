# Lead communication

## Trainings
For members of Agaric, tips on what needs to be communicated with people who have requested training can be found here: [Training leads communication document](https://gitlab.com/agaric/trainings)
