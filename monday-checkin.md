# Monday Checkin

The Monday checkin ensures everyone is on the same page starting the week.  It is:

  * No longer than one hour, maximum.
  * Has a facilitator.
  * Has a designated notetaker, with everyone encouraged to participate.
  * Tasks should be [added to GitLab](https://gitlab.com/agaric/internal/-/boards/) as the meeting takes place.

Here is a template that can be pasted into a text pad (ideally markdown-aware).

```md
# 2023 MONTH XXth – Monday Checkin

## Checkins

*Quick updates on personal headspace, and work updates from today or things from weekend *

  * Mauricio
  * Chris
  * Micky
  * Ben
  * Keegan
  * Louis

## Leads, or important projects

*Remember to discuss scheduling for any leads meetings*

### Hours Entered (previous week)

  * Ben - 
  * Chris - 
  * Keegan - 
  * Mauricio - 
  * Micky - 
  * Louis -

## Pair programming avilability for the week 

  * Ben - 
  * Chris - 
  * Keegan - 
  * Mauricio - 
  * Micky - 
  * Louis

* Drutopia Office Hours - anything to do with Nedjo this week?

### Project assignments

* MASS Design Group - MASS Continuous Improvement (Ben/Dave)
* Portside - Portside (Ben/Chris)
* UC Irvine - PECE (Ben/Keegan)
* Vermont Housing Finance Agency - VHFA (Chris/Ben)
* HousingWorks, Inc. - HousingWorks (Louis/Ben)
* Teachers with GUTS - Project GUTS/TWIG/Making Sense of Models (Ben/Louis)
* Sahara Reporters - Sahara Reporters site migration (Ben/Sanjay)
* NC Housing Finance Agency - NCHFA Maintenance (Louis/Ben)
* Agaric e.K. - C-Team support (Zeit) (Mauricio/Louis)
* UPenn - Jacket2 Upgrade to D10 (Mauricio/Keegan)
* Kalamuna - Outsourcing (Mauricio/Ben)
* CRLA - CRLA.org Development & Support (Keegan/Ben)
* Action Information - Green Calendar (Sanjay/Louis)
* Grassroots Economic Organizing (GEO) - GEO Support (Ben/Louis)
* Eliot School of Fine & Applied Arts - Eliot School Site & CRM (Keegan/Ben)

Only listed @ score of 2+ for others see https://share.mayfirst.org/f/11573025

## Blockers

  * Mauricio
  * Chris
  * Micky
  * Sanjay
  * Ben
  * Keegan
  * Louis

## Task allocation

*List tasks throughout planning session, by end each task should have a person assigned to it.*
```
