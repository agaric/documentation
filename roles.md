# Roles

## Rotating cooperative-wide roles

In the interest of distributing types of work equitably and skill-sharing, a number of tasks ideally rotate— weekly, or monthly, or somewhat randomly.

### Meeting facilitator

 * Standup facilitator (all regular meetings)
 * Monday/Friday planning facilitator (can be different than facilitator in standup part).
 * Worker-owner meeting facilitator

In practice standup facilitator has been Keegan 95% of the time, with Chris and Sanjay a bit more likely to pick up the planning facilitation.

### Timecop

* Checks to ensure people get their time in each day by the end of Monday planning, mid-week daily standup, and Friday shipping.  Works with people individually after the meeting to get the time in.

In practice, we have not done this for a while.

### Scribe

* Write one sentence about each person's main activity in a week, summarizing our activities in an accessible way in blog posts such as "The Week That Was: Agaric's March 30th to April 3rd"
* As our main storyteller for the month, think about ways to weave the work we're doing every week into a narrative; if we can't fit our work into a narrative we're probably straying from our strategic goals.

Never put into practice and we really need to.

### Trainer/Educator

* Write informative blog posts
* Create curricula for trainings
* Submit speaking and training sessions
* Speak/training at events
* Help organize trainings, camps and conferences  

Primary: Mauricio, Chris  
Secondary: Keegan, Ben

### Infrastructure

* Create and support working development environment for each project
* Create and support staging site for each project
* Maintain deployment workflows for each project  

Primary: Chris  
Secondary: Louis, Ben 

### Leads

* Cold calls (we have never done this)
* Future event scanner - list events of interest in advance
* Respond to requests received by contact form or email
* Write proposals
* Coordinate estimates
* Manage marketing-oriented pages on website   

## Marketing

* Write blog posts
* Post to social media
* Coordinate sponsorship of events
* Network at events
* Take pictures and video
* Promote events we are part of  

## Contractor Relations

* Communicate hours and progress with contractors
* Make sure contractors send invoice each month
* Make sure contractors get paid each month
* Check in on how contractors are feeling about their work and projects
* Check in with Agaric team on how things are going with contractors they work with  

Currently making no effort to rotate.

Primary: Sanjay  
Secondary: Micky

### Team Management

* Review planned time usage on a weekly basis (start of week, based on reports in Monday team tempo meeting)
* Make suggestions to try to ensure Agaric worker-owners combine for at least 60 billed hours a week (60x4x150=$36K/mo=clearing payroll). 
  * Works with project leads to find recommended tasks.
* Pairs with contractor relations for overall use of available Agaric time.
* Implement Team Tempo process  

Unfilled since Clayton left.

## Project-specific roles

### Lead

#### Client Relations

* Hold monthly [sensing](https://gitlab.com/agaric/internal/wikis/meetings/sensing) and sprint planning/review meetings
* Send monthly ROS and project update
* Make sure payments from client are happening on time
* Update Agaric team on how a client is doing and how their projects are going in worker-owner meetings  

This role as described has not really been filled since Clayton was here.

### Developer/Designer

#### Design

* Create wireframes/prototypes
* Create design mockups
* Create styleguide using HTML and SASS  

Primary: Unfilled

#### Development

* Use contributed and custom modules to implement functionality
* Translate styleguide into working website
* Review others' code
* Test code
* Deploy approved changes

Primary: Ben, Mauricio, Louis  
Secondary: Chris, Keegan

### User Research

* Define and measure project goals and key performance indicators (KPIs
* Define key user groups
* Conduct research to learn users' needs and motivations  

### Project Manager

* Plan sprints
* Ensure issues have acceptance criteria and relevant info
* Facilitate project meetings
* Facilitate key milestones such as security updates, redesigns and launches

## Responding to Inadequate Performance of a Role's Duties

* Person noticing an issue address directly. If issue persists, call a meeting with the whole group to discuss a resolution/change.

## Suggestions for role-fluidity

* Define documentation requirements and tool usage for the role (e.g. effective use of GitLab for developers, detailed tracking in CMS tool)
* Specific email addresses for particular roles
* Document requirements of a role - hard skills, soft skills, and required resources (e.g. excellent communication, GnuCash familiarity, etc).
* "Pair-programming" approach

```{note}
Related: <a href="https://gitlab.com/agaric/internal/wikis/worker-owners">Worker Owners</a>
```
