# Project management with GitLab

Need to polish and publish our internal docs:

* https://gitlab.com/agaric/internal/wikis/Project-Management
* https://gitlab.com/agaric/internal/wikis/estimates
* https://gitlab.com/agaric/internal/wikis/version%20control

... and come to think of it, reorganize GitLab-specific notes as a subset of project management approach.

### Follow issues
Gitlab has the option to turn on notifications for each issue, however you can not necessarily filter issues in gitlab by which ones have notifications turned on.
However, you can react to issues and filter by the reactions you give. To achieve the desired filter on issues you want to follow, react with an 👂 (which will also turn on notifications).
Any time you want to view your issue watch list, go to this link: `https://gitlab.com/dashboard/issues?scope=all&state=opened&my_reaction_emoji=ear`.  

Tah dah! You can now follow issues in gitlab!

### Disable autoclosing issues from commits and merge requests

Under Repository Settings (Settings » Repository, for instance [`gitlab.com/agaric/sites/agaric-com/-/settings/repository`](https://gitlab.com/agaric/sites/agaric-com/-/settings/repository)) expand the **Default Branch** section and uncheck **Auto-close referenced issues on default branch**.

```{note}
See [GitLab help: Disabling automatic issue closing](https://gitlab.com/help/user/project/issues/managing_issues.md#disabling-automatic-issue-closing)
```

