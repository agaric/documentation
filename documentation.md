# Documentation

Welcome to the meta section of our documentation where we document how to do documentation!

Edit or add.  That's the primary instruction.  You can edit directly on [gitlab.com/agaric/documentation](https://gitlab.com/agaric/documentation) (for example, the edit link on this page takes you to [gitlab.com/agaric/documentation/blob/main/documentation.md](https://gitlab.com/agaric/documentation/blob/main/documentation.md)).  If you aren't a member of Agaric, GitLab will helpfully offer to fork the documentation to your own namespace so that you can make a merge request with your documentation suggestion.

```{tip}
This documentation page is a good one to copy or refer to for an example of MyST formatting.  And of course anyone can come and clean up formatting later.
```

## Where to post what


We like [Gitlab's approach](https://about.gitlab.com/handbook/git-page-update/#where-should-content-go): If you're not sure where to put something in documentation, or if it even is documentation, [write a blog post](https://agaric.com/node/add/blog).  Or even, in the Agaric context, just throw it in a [raw note](https://gitlab.com/agaric/raw-notes) (this private repository automatically publishes non-draft notes publicly to [agaric.gitlab.io/raw-notes](https://agaric.gitlab.io/raw-notes/)).

Somewhere is better than nowhere.

Don't worry about [translation](translation.md).

## Guidelines

* Document closest to where people work:
   * In README.md files or otherwise in the code repository for developers.
   * In the site itself for site managers and content editors, or alternatively in an organization's already established locations.
* Topics which are of general application can be abstracted, put in this repository, and linked to at this documentation.

```{note}
In doing documentation we are living our [values](values.md) of encouraging continuous learning, appreciating new ideas, giving back to the communities we are part of, and valuing long-term relationships.
```

## This documentation

 * Written primarily in Markdown, enhanced by Sphinx.
 * Uses the [MyST parser for cross-referencing](https://myst-parser.readthedocs.io/en/latest/syntax/cross-referencing.html).

### Local preview

#### Setup

```bash
sudo apt install python3-sphinx
pip3 install -r requirements.txt
```

#### Generating

Running this documentation locally:

```bash
sphinx-build -b html . _build/html
```

### Useful examples

#### Including the table of contents of another page

```
```{toctree}
---
caption: See also
---

copyright-and-trademarks
```
```

Produces this:

```{toctree}
---
caption: See also
---

copyright-and-trademarks
```
