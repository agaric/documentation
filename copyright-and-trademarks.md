# Copyright and Trademarks

Copyright is a bundle of exclusive legal rights that vary depending on the type of work. A copyright owner can grant some or all of those rights to others through a license. This page documents Agaric's approach to copyrights, trademarks, and Creative Commons licenses.

### Basics

Copyright protection applies to any original works that are fixed in a tangible medium. This includes works like drawings, recordings of a song, short stories, or paintings, but not something like a garden, since it will grow and change by nature. Copyright does not cover facts, ideas, names, or characters.

Copyright protection begins when the work is first created and it doesn’t require any formal filings. However, to enforce a copyright in the US, you need to register the work with the US Copyright Office. (For further clarity, check out their FAQ page, which is full of gems like “How do I protect my sighting of Elvis?”)
Copyright notice on the work is not required but it is recommended, since it cuts off a defense of innocent infringement.

### Copyright at Agaric

We default to a Creative Commons license whenever possible.

### Other creators’ copyrights

We respect the copyright of other creators. If we want to use someone else's copyrighted work, we have to obtain a license from the owners.  
A copyright license spells out these terms:

 - Where we can use the work  
 - How long we can use it for  
 - How much we'll pay them for the use  
 - Whether or not we’re the only ones who can use the work  
 - What we can do with the work  
 - Any restrictions on our use (for example, that we can use it online but not on a billboard)

For example:

"You grant Agaric a perpetual, worldwide, non-exclusive, royalty free license to display, distribute, and publish the Work in our marketing in any medium now known or later developed."


### Image use and copyright

Agaric prefers using original images in our blog posts.  Ben has a lot of random images and you can check with him on subjects.  We also have a relationship with [Martin Owens to commission digital vector art made with Inkscape](https://www.patreon.com/doctormo).

If you use an image, photo, or other design element made by someone outside Agaric, get permission first. Once you have permission, always give the copyright owner credit and link back to the original source.

Attribution goes for Creative Commons also, and we have an attribution field built into our website for blog posts.

Good sources of Creative Commons or open access (public domain) images include:

 - Flickr: https://www.flickr.com/search/?license=4%2C5%2C6%2C9%2C10
 - Smithsonian Institute: https://www.si.edu/openaccess

### Other licenses

#### Creative Commons licenses

Instead of the standard “all rights reserved,” some creators choose to make their work available for public use with different levels of attribution required. That’s what we’ve done with this style guide. Find a breakdown of licenses on the Creative Commons website.
We love to share our work and use these licenses frequently.


### Trademarks

A trademark, often called a mark, can be a word, name, sign, design, or a combination of those. It’s used to identify the provider of a particular product or service. They’re usually words and images, but in some cases, they can even be a color.

To be protectable, a trademark needs a distinctive element. There’s a “spectrum of distinctiveness” that spans from inherently protectable marks to ones that require additional proof to ones that may never be protected.

Fanciful marks, which are made up words like Kodak or Xerox, are the most easily registered and protected.  (Drutopia!)

Arbitrary marks, which are words which are used out of context like Apple or Sprite, are also easy to protect.  (Agaric!)

Suggestive marks, which suggest at some element of the goods or services like Greyhound, follow.

Descriptive marks, where the word's dictionary meaning aligns with the goods or services offered, like Mr. Plumber or Lektronic, are not protectable unless they develop a secondary meaning. That means a consumer would immediately associate the mark with only that good or service. This can be hard to prove, so it's best to avoid descriptive marks when possible.

Generic terms, or the common name for a product or service, are not protectable.
We classify Agaric as an arbitrary mark.  We aren’t too concerned with the trademark status of Agaric.

#### Displaying trademark notices

To note that something is a trademark, and in the case of registered marks in order to collect damages, the trademark has to be displayed with an appropriate symbol.

Here are the various trademark symbols and when to use them:  
* For unregistered trademarks of goods, use ™  
* For unregistered trademarks of services, use ℠  
* For trademarks granted registration by the United States Patent and Trademark Office, use ®  
* Note that using ® on marks that haven’t been registered by the USPTO can be considered fraud, so if you’re not sure if a trademark is registered, don’t use ® .  
* The trademark symbol should appear as close to the mark as possible.  
