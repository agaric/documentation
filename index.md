# Welcome to Agaric Collective's documentation!

```{image} images/agaric-logo-horizontal.svg
:width: 240px
:alt: Agaric logo (two mushrooms reminiscent of the cooperative trees)
:align: center
```

[Agaric](https://agaric.coop/) builds software to give people the most power possible over their own lives.

## General Instructions

* Don't panic.
* Be kind.

```{toctree}
---
caption: The Agaric Way
maxdepth: 2
---

purpose
values
cooperative-principles
```

```{toctree}
---
caption: Day-to-day
maxdepth: 2
---

roles
intra-team-communication
lead-communication
calendars
project-management-with-gitlab
weekly-rhythm
days-off
```


```{toctree}
---
caption: Tools of the trade
maxdepth: 2
---

tools/git-setup
tools/git-usage
tools/setting-up-nextcloud
tools/securely-sending-files-nextcloud
tools/setting-up-email
tools/big-blue-button
tools/recommended-local-project-locations
tools/creating-new-drutopia-site
tools/deploying-drutopia-updates
tools/ddev-local-development-environment
tools/drutopia-member-server-access
tools/upgrading-drutopia-platform-elizabeth-sites
tools/uptime-monitoring
tools/inspecting-logs
templates/drupal-module-project
```

```{toctree}
---
caption: Writing for Agaric
maxdepth: 2
---

content-style-guide
agaric-website/basics
agaric-website/agaric-site-content-entry
agaric-website/tags-and-taxonomy-terms
agaric-website/translation
agaric-website/short-urls
marketing
copyright-and-trademarks
```


```{toctree}
---
caption: How We Work
maxdepth: 2
---

decision-making
growth
client-communication/document-versioning-filenames
```

```{toctree}
---
caption: Documentation
maxdepth: 2
---

documentation
shared-technical-notes
reference-docs
```


---

If you've found your here way to Agaric's internal documentation but don't know about the [Agaric technology collective](https://agaric.coop/) itself yet, please check out our [development and consulting services](https://agaric.coop/services), [our trainings](https://agaric.coop/training), and [our initiatives](https://agaric.coop/initiatives) or read [about Agaric](https://agaric.coop/about-agaric) and [ask us for help or collaboration](https://agaric.coop/ask).
