# IRC

### Zulip <-> IRC bridge

This package has been installed using (sort-of) the instructions at https://agaric.zulipchat.com/integrations/doc/irc

The bot currently lives on irc.agaric.com under the zulip account with software at /srv/zulip.

To launch the bot, login as root, and then:
```
su - zulip # become user for bot for agaric
startzulip
ctrl+d # switch back to root
su - zulip-nichq # become user for bot for nichq
startzulip
```

This should start zulip in the background and you can now disconnect. If there are errant processes, use `ps aux|grep zulip` to locate PIDs and `kill {pid}` of each. Also, you can `kick bot_zulip` on IRC if you are an IRC operator, which should also kill an existing process.


### IRC Bouncer ###

**This is currently not active. There is an admin account installed/configured, but not auto-starting, and currently not running. If any member wants to restore the service for themselves, feel free!**

In order to acquire a 1.7 edition of ZNC, the backports for stretch were added and utilized for this package.

ZNC 1.7 is an IRC bouncer listening on port 1025 of irc.agaric.com.

A web interface is available for management at: https://irc.agaric.com:1025 that enables module maintenance, etc.

The entire app runs under the local system user znc-admin (just run `znc` as the znc-admin user).

### Web-based IRC client: The Lounge

We can access our IRC (and any other IRC) through:

https://irc.agaric.com

#### The Lounge Management

[The Lounge is a self-hosted web IRC client](https://thelounge.github.io/) we're using to provide https://irc.agaric.com

Everyone in [SSH_public_keys](https://gitlab.com/agaric/internal/-/wikis/SSH-Public-Keys) as of 2017 August has access to the Digital Ocean droplet (1cpu/512mb) hosting it:

`ssh root@irc.agaric.com`

##### Upgrading

TheLounge is installed manually via dpkg as there is no current apt source for it.

To upgrade via dpkg:
- Go to https://github.com/thelounge/thelounge/releases/ and select your release
- Copy the link to the deb file at the bottom of the releases page.
- `ssh root@irc.agaric.com` and run `wget -L -o thelounge.deb <copied link>`
- Install the new package with `dpkg -i thelounge.deb`
- The service should restart, but if it does not, `systemctl restart thelounge`

There is also an option to use `thelounge update` but it is unclear if this is preferable to using dpkg.

##### Configuration

TheLounge is bound to the loopback address at port 9000 and reverse proxied via NGINX. The configuration and user files are located at `/srv/lounge/`. The configuration is pointed at via an Environment variable (the variable can be inspected/changed via `systemctl edit thelounge`).

##### TLS(/SSL)

NGINX will bounce HTTP connections to HTTPS and handle encryption via reverse proxy. Certbot is installed and should be handling automatic renewals with reload of NGINX as needed.

##### Management

Note that because the ENV var is depended upon for our configuration, and it runs as thelounge user. In order to facilitate simpler management, an alias is defined for thelounge under the root account.

This is configured for you (as root):
```
alias thelounge='sudo -u thelounge THELOUNGE_HOME=/srv/lounge thelounge'
```

List users:

```
thelounge list
```

Addin a user:

```
thelounge add username
```

Where `username` is the IRC nick for the user you are adding.

reset pwd:

```
thelounge reset username
```

Additional management commands can be found in [TheLounge documentation](https://thelounge.chat/docs/users).

### IRC Bot (Limnoria aka Supybot)

#### History

Stefan Freudenberg selected and installed Supybot as Agaric's general-purpose information bot (in particular expanding issue numbers to issue titles) circa 2010. Early in 2018 Chris Thompson upgraded Supybot — "a robust (it doesn't crash), user friendly (it's easy to configure) and programmer friendly (plugins are extremely easy to write) Python IRC bot. It aims to be an adequate replacement for most existing IRC bots. It includes a very flexible and powerful ACL system for controlling access to commands, as well as more than 50 builtin plugins providing around 400 actual commands" — to Limnoria — a project which continues development of Supybot.

It is installed on Simone.

#### Common commands

@later tell username A message

(until we override @tell to use the far more useful 'later' flavor)

#### Official documentation

* https://github.com/ProgVal/Limnoria
* http://doc.supybot.aperio.fr/en/latest/index.html

