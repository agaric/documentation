# Deploying Drutopia updates

## Prerequsites

Ensure you have at least PHP 7.2.5 installed. An 8.x version is recommended.

```shell
sudo apt-get install ansible rsync php8.1-cli
```

Then follow the commands from:

[getcomposer.org/download](https://getcomposer.org/download/)

Including the recommended:

```shell
sudo mv composer.phar /usr/local/bin/composer
```

To make working with [Drutopia Platform's recommended Ansible setup](https://gitlab.com/drutopia-platform/drutopia_host#introduction) easier (and for the following instructions to work), install Ahoy per its instructions:

[github.com/ahoy-cli/ahoy](https://github.com/ahoy-cli/ahoy)

Get yourself added to the [Drutopia Platform project on GitLab](https://gitlab.com/groups/drutopia-platform/-/group_members) if you are not already, and [ensure your public SSH key is on Gitlab](https://gitlab.com/-/profile/keys).

Finally, [clone needed Drutopia repositories locally according to the recommended setup](creating-new-drutopia-site).

Now you are ready for deploying Drutopia updates on a regular basis.

## Configure live to reach test instance

**These instructions are no longer recommended. The Drutopia member role will set up the SSH connection along with the sync script (`sync_to_test.sh`).**

Log into the server on the live side, and check if you can reach the test side from there:

```shell
ssh {site}_live@drutopia.org    # If using our ssh-config: d-{site}-live
ssh {site}_test@drutopia.org    # d-{site}-test is not available here!
# If that command fails, create an ssh key:
ssh-keygen
cat ~/.ssh/id_rsa.pub
```
Copy the output of the cat command and disconnect (ctrl+d). Then add that public key to the authorized_hosts on the test side:

```shell
ssh {site}_test@drutopia.org
vi ~/.ssh/authorized_hosts
```
`shift+g` to get to the bottom, `o` to get into add mode on a new line, `ctrl+shift+p` (or appropriate) to paste the key, then `{esc}` to exit insert more, and `:wq` to write changes and quit.

Re-test reaching the server from the live side. This time you will have to accept the host key verification for drutopia.org which should be: `SHA256:MQXYY1PcuEgnIdyYawJSNZHbvLMwBXOx5CyDBvNSBmI.`

## Perform a sync to test

Log into the live version of the site and perform a sync of the db and files to the test instance:

```shell
ssh {site}-live@drutopia.org    # If using our ssh-config: d-{site}-live
sync_to_test.sh
```

## Ensure you are up-to-date with all hosting repositories

```shell
cd ~/Projects/drutopia-platform/drutopia_host/hosting_private
ahoy git-pull-all
```

## Creating new site hosting entries

You can use ahoy to build a templated YAML snippet for yourself:

```shell
ahoy new-site example
```

Following the instructions this command provides to pull the newly created file into the vault.

## Finding the site name and identify builds

```shell
## Determine the build to deploy
ahoy vault-view
# Without ahoy:
ansible-vault view host_vars/elizabeth.mayfirst.org/vault.yml
```

Search for the site you are deploying. The `drutopia_version: ` key will identify the build to deploy in the next step.

If using typical `sitename-test.drutopia.org` and `sitename-live.drutopia.org` domains pending the real site domain, add the subdomains to drutopia.org through the [May First control panel](https://members.mayfirst.org/cp/)

## Prepare the appropriate base build

Using ahoy, specify the name of the build to create as an argument to `ahoy deploy-build`:

```shell
ahoy deploy-build next
```

## Deploy your site

```shell
ahoy deploy-site example_test
```

And you can then share back the record of the deployments in the `build_artifacts` repository with:

```shell
ahoy artifacts
```

## Putting it all together

```shell
cd ~/Projects/drutopia-platform/drutopia_host/hosting_private
ahoy git-pull-all        
ahoy deploy-build stable
ahoy deploy-site example_live
ahoy artifacts
```

## If you need to override site configuration:

```shell
ahoy deploy-site-force example_test
```

## Sync live database to test

If you have new content on the live site that you want to see how your code works with, or if you have created entities on the test site that block the removal of configuration you changed your mind about, you will want to sync from live to test.

```shell
ssh d-example-live
sync_to_test.sh
```

This handles making a paranoia dump of the test site in `~/backups`, dropping the test database to ensure no tables are left to clutter and interfere, skipping the content of cache tables, and bringing over user files (skipping cache files like the twig folder).

## Bonus: Keep Drutopia builds with similar available modules

To try to keep various Drutopia-based distributions from diverging too much, at least insofar as available modules, even if they aren't installed, we can use the **meld** (`sudo apt-get install meld`) diff tool to compare and share when posssible.

```shell
meld ~/Projects/agaric/sites/crla/crla-org/composer.json ~/Projects/agaric/sites/geo/composer.json ~/Projects/drutopia-platform/build_source/composer.json ~/Projects/agaric/sites/agaric-com/composer.json
```

When these align in not needing special patches or versions, we can consider dropping a custom build in favor of collaborating on a single one.
