# Setting up e-mail

With the exception of the note below, follow the [May First documentation on how to configure Thunderbird](https://support.mayfirst.org/wiki/faq/email/setup-thunderbird) or [other e-mail clients](https://support.mayfirst.org/wiki/faq/email/pop-vs-imap).

For Server Name, Agaric team members use `mail.mayfirst.org` for incoming e-mail (POP or IMAP) and the May First standard, `mail.mayfirst.org` for outgoing (SMTP).

For extra security, Agaric worker-owners are advised to set up a separate account for e-mail as for administrative duties within May First's control panel.  A separate account could also be used for [Nextcloud](setting-up-nextcloud).  This is probably unnecessary complexity for most clients.  One account can be used for all services available through May First.
