# Upgrading Drutopia sites on the Elizabeth Drutopia platform

  1. Ensure BAT and BEE are not in use, and are uninstalled.
  1. Ensure Markdown is not in use, and uninstalled.
  1. Replace local composer.json with [~/Projects/drutopia-platform/build_source/composer.json](https://gitlab.com/drutopia-platform/build_source/-/raw/master/composer.json)
