# BigBlueButton

[BigBlueButton](https://docs.bigbluebutton.org/)

To mute with keyboard:

options menu (three vertical dots in the upper right hand corner).

`Alt + Shift + M`


```{note}
  There's an [issue to float mute/unmute](https://github.com/bigbluebutton/bigbluebutton/issues/9947) which will be helpful when screensharing; until then using the shortcut is crucial.
```
