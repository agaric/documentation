# Drutopia Member Server Access

Agaric manages a large number of Drupal, primarily [Drutopia](https://drutopia.org/) sites on [MayFirst Movement Technology](http://mayfirst.coop) infrastructure.

For clients that require or desire access to their instance in order to perform their own backups, we provide SSH access, which includes secure transfers via sftp, scp, and rsync.

## Getting access

Access to your instance and data is made possible via SSH authentication. We only offer key-based authentication to SSH. This means you must create your own public/private key pair, and then provide your public key to your Drutopia administrator. (Once you have access via one key, you may modify the keys allowed as you need to by modifying your `~/.ssh/authorized_keys` file.)

Directions for generating a key pair for various operating systems can be found in many places online (i.e. search "ssh keygen"). One example is at [ssh.com](https://www.ssh.com/academy/ssh/keygen). An example of other methods for Windows systems, such as [Git bash](https://docs.joyent.com/public-cloud/getting-started/ssh-keys/generating-an-ssh-key-manually/manually-generating-your-ssh-key-in-windows) are also available.

SSH keys come in pairs - a public and private key. Your private key is just that: private. It should not be shared outside your system. Common file names for this key are, for example: `id_rsa` or `id_ed25519`, depending on the type of key you choose to generate. The public portion is the one you share with the administrator, to be added to your account on the Drutopia host server. The public portion is typically suffixed with .pub, e.g. `id_rsa.pub` or `id_ed25519.pub`.

## Logging in

Once you have a public key configured by the Drutopia admin, you are able to connect with ssh and sftp. SSH access enables you to get a remote terminal prompt on the server to issue commands, such as attaching to your database or running drush. Your Drutopia admin will provide your user name, and the host name will match your web site's address. The convention for instances on the site is that an `<site>_test` and `<site>_live` user exist for each, corresponding to that instance of your site.

For example, for a site called 'example.com', you might login using:

```sh
ssh example_live@example.com
```

## Common operations (drush, mysql, backups)

Once you have logged in, the environment should be pre-configured for you to have quick access to your database and drush commands. Simply entering `drush` will list a set of commands available. Similarly, typing `mysql` will automatically log your into the corresponding database.

We'll perform the two most common operations - you can enter these commands as they are immediately after logging in via SSH.

A common task to perform at the prompt is to make a database backup, or to create a tarball (like a "zip file") of your files folder. This can be accomplished a few ways, but here's an example using drush to create the file `backup_db.sql.gz` in your home folder (indicated by `~`):

```sh
drush sql-dump --gzip --result-file=~/backup_db.sql
```

If you do not have gzip on your system, you can omit the `--gzip` argument, but note the file may be significantly larger and take longer to transfer from the system. The file created in this case will be `backup_db.sql` (i.e. no .gz at the end). Linux and Mac systems should be able to handle gzip files (gunzip <file>). For Windows systems, you might want to try [7zip](https://www.7-zip.org/).

In order to create a single file called `backup_files.tgz` in the home folder, containing the full contents of your public/uploaded files folder:

```sh
tar czf backup_files.tgz files/
```

Once these have been created, you can transfer them via sftp.

NOTE: Please be courteous with regard to your use of space/creation of multiple backups on this server - storage space is somewhat limited, and currently quotas are not being enforced.

## Transferring files

To transfer files locally, you can use a command-like client for scp or sftp. Here is an example using an sftp client, where `ls` command lists files, `get` is used to pull the file locally, and finally `exit` quits the session:

```
sftp example_live@example.com
Connected to example_live@example.com.
sftp> ls
backup_db.sql.gz           backups                    bin                        config                     custom                     files                      logs                       site
sftp> get backup_db.sql.gz
Fetching /home/example_live/backup_db.sql.gz to backup_db.sql.gz
/home/example_live/backup_db.sql.gz                                                  100%   27MB   2.9MB/s   00:09
sftp> exit
```

Or, more directly with scp, download the backup file to the current directory (as indicated by the `.`):

```
scp example_live@example.com:backup_db.sql.gz .
```

For Windows, [WinSCP](https://winscp.net/eng/index.php) is an often-recommended graphical client.

As mentioned previously, `rsync` is also available for syncing to/from the server.

Note there is little direct benefit to syncing your `~/site` folder, but this does contain the full code of the active installation. While you *can* theoretically do so, it is preferrable to work from your git source should you want to deploy your site elsewhere. Installations of Drupal on Drutopia are controlled in such a way as only certain builds of the Drutopia codebase can be installed - the contents of the site folder are not user-modifiable. ~/config and ~/site/themes/custom folders are sourced from your git repository during site install/upgrade by the Drutopia admin.