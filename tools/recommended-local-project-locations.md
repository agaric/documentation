# Recommended local project locations

We recommend that the locations for projects on the computer you use for development follow the same pattern as other developers so even commands we share can work without modification.

## Initial setup

```bash
mkdir ~/Projects
```

## Each project

Get the directory structure directly from the paths in the GitLab.com URL.  For example, 

## Non-GitLab projects

For non-GitLab projects, we recommend

```bash
mkdir ~/Projects/github
mkdir ~/Projects/drupal
```

etc.
