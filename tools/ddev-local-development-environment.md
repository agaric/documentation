# Local development environments with ddev

We are broadly moving to [ddev](https://ddev.readthedocs.io/en/stable/) from Vagrant.

## Installing ddev

First, install [Homebrew](https://brew.sh/).

Then:

```
brew tap drud/ddev && brew install ddev
```

## Updating ddev

```
ddev poweroff && brew upgrade ddev
```

Don't forget the poweroff part or it breaks SSH until you run it.

## Using ddev

See [creating new Drutopia sites](creating-new-drutopia-site) for per-site ddev configuration.

## See also

  * [Git setup](git-setup)
