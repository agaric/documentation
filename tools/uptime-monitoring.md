# Uptime monitoring

We presently monitor that our sites and services are running correctly with [Uptime Kuma](https://uptime.kuma.pet/).

A public status report for Agaric sites is available here:

https://monitor.ocean.agaric.coop/status/agaric
