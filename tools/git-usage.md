# Git usage

## Getting in sync with other developers

To bring in changes from other developers into the branch you are
working on, from main `git pull` and then `git checkout your-branch-name` and `git rebase main`

## Git troubleshooting

If you've accidentally made commits to main and cannot pull in other's changes as a result.

To get rid of a commit (the most recent one),

```
git reset HEAD^
```

to then be able to `git stash` changes from the commit, now local changes.  To just throw everything in the commit away, `git reset --hard HEAD^`

See [sethrobertson.github.io/GitFixUm/fixup.html](https://sethrobertson.github.io/GitFixUm/fixup.html)


## When NOT to manually resolve merge conflicts

When it is an automatically generated file!

For `composer.lock`, for example:

```
rm composer.lock
ddev composer update
git add composer.lock
git commit
```

## Bringing feature branches into main



It is OK to do git merges rather than rebase on top of the main branch, especially when in the GitLab UI where merge is the only option— but be absolutely certain "Squash commits" is **not** checked.


### Discussion

Hmm git GUIs would show all the commits in the branch they were worked on when there are merge requests, right? (if not squashed?) Maybe better to prefer merges than rebases for feature branches, for preserving the history of where the work was done. My problem with git merge commits is they can rewrite history inside them, like have changes not shown in any other commit, and they make it hard to see what happened— does not show up in git log -p for instance. (You can see the history but if the stack overflow explanation does not fit on one page i don't want it as part of my daily workflow. https://stackoverflow.com/a/40986893/1028376 )
