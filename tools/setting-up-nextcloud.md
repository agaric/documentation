# Setting up Nextcloud for a client

There are several non-intuitive steps to get Nextcloud set up and gotchas which clients have to know about.

It is not possible to add accounts or change passwords in May First's Nextcloud instance, [share.mayfirst.org](https://share.mayfirst.org/).  This must be done in May First's control panel.

Log into [May First's control panel](https://members.mayfirst.org/cp/) with the organization's May First account and create a new User Account to use exclusively for Nextcloud, for instance `exampleorg-nextcloud`, with a strong password.  It is not necessary to provide an e-mail address.

Also here at the **User Account** vertical tab, create user accounts for any people who will be using Nextcloud and do not already have May First accounts (either in the organization *or* anywhere else— May First accounts are universal across most tools provided by May First, including Nextcloud).  Checkmark "Add a nextcloud item to this user account"

Be sure to set the disk space quota to something that would cover each person's expected use of Nextcloud (and e-mail, if that person will be using May First's e-mail also).

Now go to the **Nextcloud** vertical tab and adjust the quota allocated for their expected Nextcloud use, if more than 1GB.  You can also add any of the user accounts for your organization to your Nextcloud for which you did not check the Nextcloud box when creating.  

0. Log into [share.mayfirst.org](https://share.mayfirst.org/) with this new Nextcloud-only account.
1. Press the + sign under **Circles** to create a new circle under [Contacts](https://share.mayfirst.org/apps/contacts/All%20contacts)
2. Type a no-spaces version of the clients name (for instance, `exampleorg`) into the box for the name at the top center.
3. Under **Invites**, probably do not checkmark anything.  Under **Visibility**, *do **not** select visible to everyone.

Ask the people for whom you created accounts on May First to use the same username and password to sign into Nextcloud at [share.mayfirst.org](https://share.mayfirst.org/) 

Once they have confirmed they have done that, add each of those people to the circle (use the same username as they have in the user account in the May First control panel).

```{admonition} You cannot share a folder or calendar with someone who has not yet logged in on share.mayfirst.org

Send an email to the client with instructions to log into NextCloud at https://share.mayfirst.org with their MayFirst username and password.

You can send  a password reset email by using the "Password Reset" vertical tab in the left hand sidebar and 

After a client has logged in to share.mayfirst.org, you will be able to share folders and files— and add them to the organization circle for access to anything you add it to.
```

Under [Files](https://share.mayfirst.org/apps/files/) on Nextcloud create a folder named after the organization (for instance `exampleorg`).  Share that folder with the personal circle you previously created, by typing the circle name in the "Search for share recipients" box in the **Sharing** tab.

Now every folder you create *within* that folder will be available to everyone in the organization (the people you added to the circle).  And even if a person has multiple affiliations on May First's Nextcloud, the files related to the organization will be clearly namespaced.

```{admonition} Further reading

  * <https://support.mayfirst.org/wiki/nextcloud>
```

## Rationale

We create an organizaton account so that it's clear people are being invited to organization resources and there is no problem if an individual leaves an organization.

Because the account will have the same password for Nextcloud and for everything else in May First, we create an account through the May First control panel that will be used only for Nextcloud.  This way credentials used, even rarely, for logging into Nextcloud are not also used to control web hosting, e-mail, and other critical resources.

MayFirst suggestions for how groups can effectively use nextcloud to share. The steps are here: https://support.mayfirst.org/wiki/nextcloud#CanIcreategroupsofpeopletosharewith

Our steps are now largely in sync with May First's own recommendations; perhaps we can merge documentation.
