# Calendars

1. Ensure the person has a [May First Movement Technology](https://mayfirst.org/ ) user account.
2. Log into NextCloud at [share.mayfirst.org](https://share.mayfirst.org/ ) and go to [Calendar](https://share.mayfirst.org/apps/calendar/ ).
3. Create your own business calendar to be shared with teammates.
4. Share the newly created calendar with the Agaric team by clicking "Share" and then typing in the usernames of each member (do not trust the autocomplete, you will likely have to type the full username before the account shows up).
5. Ask to be added to the Agaric shared calendar.

## Norms and customs

  * Do not post anything to agaric-shared that is not definitely for everyone.
  * Mute notifications on calendars other than your own and agaric-shared.


## Integrating NextCloud calendars with Thunderbird

### Subscribe to a NextCloud calendar 

0. In NextCloud web calendar, press the three dots (**&middot;&middot;&middot;**) next to the calendar (your own or a colleagues) which you'd like to have in Thunderbird and press **Copy private link**
  ![Pop-up with the appearance of a dropdown menu next to Clayton's smiling face, with the top item being Edit name and a clipboard icon with Copy private link selected.](images/getting-clayton-calendar.png)
1. In Thunderbird calendar view, right-click in the empty space below your calendar and selecting **New Calendar...**
  ![Dropdown list starting with 'Show All Calendars' with 'New Calendar...' selected.](images/add-new-calendar.png)
2. Choose the **On the Network** option and press the **Next** button
  ![A dialog to 'Create a new calendar' with two radio button options and the second, 'On the Network' selected.](images/new-calendar-on-network.png)
3. Enter your username and the calendar link copied in step 0. Leave 'This location doesn't require credentials' unchecked. The 'Offline Support' option enables calendar use when offline.  
![A dialog to 'Create a new calendar' with a username text field option, a location text field expecting a calendar(s) url, and two checkboxes 'This location doesn't require credentials' and 'Offline Support' with 'Offline Support' selected.](images/mayfirst-specific-calendar.png)
4. Choose the **CalDAV** option, select the calendar requested from the url in the previous step and press the 'Subscribe button'  
  ![A dialog to 'Create New calendar' showing a Calendar Type dropdown with, the second option, 'CalDAV', selected, and a checkbox option with the requested calendar](images/choose-caldav-and-calendar.png)
5. Give your calendar a name, sticking to the name used in NextCloud to the extent practical, and save the dialog, and you're done!
6. Ensure your calendar is associated with the e-mail address with which you want to receive invites.

### Subscribe to all Calendars at Once
You can subscribe to all calendars that have been shared with you by using the https://share.mayfirst.org rather than a specific calendar's url.

0. Repeat instructions 1 and 2 from the  'Subscribe to a NextCloud calendar' section.  
1. Add your username in the username field and 'https://share.mayfirst.org' in the location field.  
  ![A dialog to 'Create a new calendar' with a username text field option, a location text field expecting a calendar(s) url, and two checkboxes 'This location doesn't require credentials' and 'Offline Support' with 'Offline Support' selected.](images/import-all-calendars.png)
2. Your calendar and all calendars shared with you are listed in the checkboxes. Choose the calendars you would like to import into your Thunderbird calendar.  
  ![A dialog to 'Create New calendar' with checkboxes listing your calendar and all calendars shared with you.](images/choose-calendars-to-import.png)

```{note}
Members of Agaric can share their private calendar links to the [internal Agaric wiki](https://gitlab.com/agaric/internal/-/wikis/calendars ).
```

```{note}
More documentation here: https://support.mozilla.org/en-US/kb/creating-new-calendars
```
