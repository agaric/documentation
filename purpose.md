# Agaric's Purpose

**To build software and teach practices that help people gain the most power possible over their own lives.**


```{admonition} See also
   * [Our values](values)
   * The [cooperative principles](cooperative-principles)
```

```{note}
We know this work must be done collaboratively, so we also like to [talk with one another](intra-team-communication).
```
