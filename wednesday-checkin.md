# Wednesday operations

  * No longer than half-an-hour, maximum.
  * Tasks should be [added to GitLab](https://gitlab.com/agaric/internal/-/boards/) as the meeting takes place.

Here is a template that can be pasted into a text pad (ideally markdown-aware).

```md
# 2024 Juluary 19th – Wednesday Checkin

## Updates

  * Mauricio
  * Chris
  * Micky
  * Sanjay
  * Ben
  * Keegan
  * Louis

## Invoice status

### Hours Entered (previous week)

  * Ben - 
  * Chris - 
  * Keegan - 
  * Mauricio - 
  * Micky - 
  * Sanjay - 
  * Louis -

## Pair programming availability
  * Ben
  * Chris
  * Keegan
  * Mauricio 
  * Micky 
  * Sanjay 
  * Louis

## Blockers

  * Mauricio
  * Chris
  * Micky
  * Sanjay
  * Ben
  * Keegan
  * Louis

## Availability

## Task allocation

*List tasks throughout, by end each task should have a person assigned to it.*

https://gitlab.com/agaric/internal/-/boards/ 
```
